#!/usr/bin/python3

"""
This file is part of DITB.

Contains the cli interface for the dark white correction of the DISCO beam
"""
import click

@click.command()
@click.argument('images', nargs=-1, type=click.Path(exists=True))
@click.option('--dark', help='set the dark .zarr file', type=click.Path(exists=True), envvar='DARK')
@click.option('--white', help='set the white .zarr file', type=click.Path(exists=True), envvar='WHITE')
@click.option('--darkofwhite', help='set the dark of white .zarr file', type=click.Path(exists=True), envvar='DARKOFWHITE')
@click.option('--white_z', help='set the z to use in white stack', type=click.INT)
@click.option('--force', is_flag=True, help="Force to normalize even if the directory is already in the destination folder")
@click.option('--lower', type=click.FloatRange(0.01,100, clamp=True), help="Set the lower percentile of pixel value set them to zero after normalisation. (The default value is 1)", default=1)
@click.option('--smooth', type=click.BOOL, default=True, help="Smooth image of white, darks and dark of whites")
@click.option('--version', type=click.INT, default=2, help="Version of the correction algorithm (1: one step correction (power and spatial with the white normed to its stack maximum), 2: two steps correction (default), a) power from white mean b) sptatial from the white normed for each Ex. wavelength")
def darkwhite(images, dark, white, darkofwhite, white_z, force, lower, smooth, version):
    """
    🔬 Normalise images with dark (images of the camera noise),
    white (image stack of the DISCO beam) and "dark of white" images.

    The dark images should have the same number of emission filters than the
    images to normalise

    The white images could be a zstack or a single image of the DISCO SR-Beam
    recorded with the same microscope objective as the images.

    The darkofwhite image should be one image with the same acquisition time as
    the white image stack.
    """

    # Do the import in the function to speed up the cli
    from ditb.exposure.darkwhite_correction import correct_dark_white
    from ditb.filters.smooth import smooth_to_zarr
    from pathlib import Path
    import shutil

    if dark is None:
        raise NotImplementedError('No dark option is not implemented')

    if white is None:
        raise NotImplementedError('No white option is not implemented')

    if darkofwhite is None:
        raise NotImplementedError('No dark of white given')

    if white_z is None:
        white_z = 'center'

    assert version in [1,2], "Only version 1 or 2 exist"

    # Keep the name of the original white to estimate the power curve
    original_white = white

    # Do the smoothing of white, dark, and dark_of_white
    if smooth:
        print(f'🪩 Smooth dark and white stacks:')
        smoothed_names = []
        for fname in [white, dark, darkofwhite]:
            output_file = Path(fname)
            output_file = output_file.parent / output_file.name.replace('.zarr', '_smooth.zarr')

            if force:
                if output_file.is_dir():
                    shutil.rmtree(str(output_file))
            else:
                if not output_file.is_dir():
                    print(f'\n▷ smooth {fname}')
                    smooth_to_zarr(fname)

            smoothed_names += [str(output_file)]

        # Replace names to add the suffix
        white = smoothed_names[0]
        dark = smoothed_names[1]
        darkofwhite = smoothed_names[2]


    print(f'🪩 Normalize images with:')
    print(f'- dark: {dark}')
    print(f'- white: {white}')
    if version == 2:
        print(f'- white for power estimation: {original_white}')
        assert 'smooth' not in original_white.lower(), "Need to use a non smoothed white to estimate the power of the beam"

    print(f'- dark of white: {darkofwhite}')

    darkwhite_names = [Path(dark).name, Path(white).name, Path(darkofwhite).name]
    for filein in images:
        fname = Path(filein).name
        if fname not in darkwhite_names or 'normalized' not in fname.lower() or 'dark' not in fname.lower or 'white' not in fname.lower:
            out_suffix = f'_normalized_v{version}'
            output_file = Path(filein).parent / Path(filein).name.replace('.zarr', f'{out_suffix}.zarr')
            print(f'\n▷ normalize {filein} to {output_file}')

            if force:
                # Remove the directory if it exists
                if output_file.is_dir():
                    shutil.rmtree(str(output_file))
                try:
                    correct_dark_white(filein, dark, white, darkofwhite, white_z, lower_percentile=lower, version=version, original_white=original_white)
                except Exception as e:
                    print(f'Error for {filein}')
                    print(e)

            else:
                if not output_file.is_dir():
                    try:
                        correct_dark_white(filein, dark, white, darkofwhite, white_z, lower_percentile=lower, version=version, original_white=original_white)
                    except Exception as e:
                        print(f'Error for {filein}')
                        print(e)

    print('\n🪩 Done 🌟🌟🌟')
