#!/usr/bin/python3

"""
This file is part of DITB.
"""

import dask.array as da
import numpy as np
from skimage.util import img_as_float, img_as_uint
from typing import Union
from pathlib import Path
import logging
arraytype = Union[np.array, da.Array]
pathtype = Union[str, Path]
_log = logging.getLogger(__name__)


def normalize_illumination_beam(white_image: np.array, dark_of_white_image: np.array = None,
                                limit_min_ratio: bool = True) -> arraytype:
    """
    Normalize the illumination beam. This function will compute:
    Normed_illumintaion = (white_image - dark_of_white_image) / max(white_image - dark_of_white_image)
    
    
    If limit_min_ratio is True (default), compute the minRatio = white_image.min()/max(white_image - dark_of_white_image) 
    and replace Normed_illumintaion < minRatio with minRatio

    Parameters
    ----------
    white_image, array (or dask array):
        the array with white image stack (could be a z-stack)
        
    dark_of_white_image, array (or dask array):
        the array with the dark of white image (i.e. the camera noise for the 
        exposure time used to acquire white images)
        
    return 
    ------
    
    the normalized illumination beam as a float image
    """
    
    float_w = img_as_float(white_image) 
    float_dow = img_as_float(dark_of_white_image)

    diff_w = float_w - float_dow 
    max_w = diff_w.max()
    minRatio = float_w.min() / max_w

    if limit_min_ratio:
        corrected_w = diff_w / max_w
        corrected_w[corrected_w < minRatio] = minRatio
    
    return corrected_w
    
    
def normalize_dark_white(image_stack: arraytype, dark_stack: arraytype, 
                         white_image: arraytype, dark_of_white: arraytype) -> arraytype:                     
    """
    Normalize the image stack using the following normalization:
    
    Normelized_stack = Image_stack - dark_stack / Normalized_illumination
    
    with Normalized_illumination = normalize_illumination_beam(white_image, dark_of_white)
    
    Parameters:
    -----------
    
    image_stack, numpy or dask array:
       The stack of images that need to be normalized
        
    dark_stack, numpy or dask array:
        The stack of darks images, they should have the same number of filters 
        than the image_stack
        
    white_image, numpy or dask array:
        The image of the selected white
        
    dark_of_white, numpy or dask array:
        The dark image of the white image
    """
    
    # dark_stack could be of dimension [filter, y, x] or as the same dimension as 
    if len(dark_stack.shape) == len(image_stack.shape):
        assert image_stack.shape[-4] == dark_stack.shape[-4], f"Number of white filters {image_stack.shape[-4]} is different of number of darks filters {dark_stack.shape[-4]}"
    else:
        assert image_stack.shape[-4] == dark_stack.shape[0], f"Number of white filters {image_stack.shape[-4]} is different of number of darks filters {dark_stack.shape[0]}"
    
    # Check type of image stack and convert it to float
    if image_stack.dtype not in [np.float32, np.float64]: 
        _log.info('Convert image_stack to float')
        if isinstance(image_stack, da.Array):
            image_stack = image_stack.map_blocks(img_as_float, dtype=np.float64)
        else: 
            image_stack = img_as_float(image_stack)
            
    # Check type of normalize_dark_white stack and convert it to float
    if dark_stack.dtype not in [np.float32, np.float64]: 
        _log.info('Convert dark_stack to float')
        if isinstance(dark_stack, da.Array):
            dark_stack = dark_stack.map_blocks(img_as_float, dtype=np.float64)
        else: 
            dark_stack = img_as_float(dark_stack)
            
    # Get the normalize_illumination_beam
    norm_beam = normalize_illumination_beam(white_image, dark_of_white)
    
    # Create the normalisation operation 
    
    # Difference with the dark
    diffdark = image_stack - dark_stack
    
    # If negartive values --> put the to zero (float image range from 0 to 1 when they come from unsigned int)
    diffdark[diffdark < 0] = 0
    
    normed = diffdark / norm_beam
    if normed.dtype != np.uint16: 
        if isinstance(normed, da.Array): 
            normed = normed.map_blocks(img_as_uint)
        else:
            normed = img_as_uint(normed)
            
    return normed