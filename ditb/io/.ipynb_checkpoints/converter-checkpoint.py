#!/usr/bin/python3

"""
Functions to convert images from one format to another.
"""
import zarr
from pathlib import Path
from ome_zarr.writer import write_image
from ome_zarr.io import parse_url
from tqdm import tqdm

from .metadata import (make_global_metadata, get_pixel_size, get_roi_name_indices, reshapeMetadata,
                       imagej_style_metadata, omero_style_metadata)
from .imread import mmread


def MM2Zarr(mm_root_folder: str, destination_folder: str):
    """
    Convert MicroManager seperate file acquisition to an ome-zarr with the following tree:
    
    Roi:XX
     |--Tile:YY
         |--0
             |-- array(time, channel, z, y, x)
    
    The metadata of micromanager are alose stored with the same arborescence for each roi/tile
    
    Parameters:
    -----------
    
    - mm_root_folder, str:
        The path to the root folder of the MicroManager acquisition
        
    - destination_folder, str:
        The path to the destination folder
    """
    
    __version = 0.1
    
    # Read metadata and images from MicroManager separate file acquisition
    metadata = make_global_metadata(mm_root_folder)
    data = mmread(mm_root_folder, metadata)
    
    # Get informations from metadata
    pixum = get_pixel_size(metadata)
    roi_indices = get_roi_name_indices(metadata)
    scale_omezarr = {'type': 'scale', 'scale': [1.0, 1.0, 1.0, pixum, pixum]}
    mm_metadata_list = reshapeMetadata(metadata)

    # Create the zarr file (remove if already exist)
    new_name = f'{str(Path(destination_folder)/Path(mm_root_folder).name)}.zarr'
    print(f'save to {new_name}')
    # Create a zarr location for the data
    zlocation = parse_url(new_name, 'w')
    root_group = zarr.group(zlocation.store, overwrite=True)

    # The case multi-tile data
    total_roitile = sum([len(roi_indices[k]) for k in roi_indices])
    with tqdm(total=total_roitile) as pbar:
        for roi in roi_indices:
            roi_group = root_group.create_group(f'Roi:{roi.replace("roi", "")}')
            for t in range(len(data[roi_indices[roi]])):
                tile_group = roi_group.create_group(f'Tile:{t}')
                write_image(data[roi_indices[roi][t]], tile_group, None, axes='tczyx', 
                            coordinate_transformations=[[scale_omezarr]])
        
                # For imageJ (support of N5 pluggins metadata, need to add metadata to the root)
                ijmetadata = imagej_style_metadata(metadata, tile_group.name)
                for k in ijmetadata:
                    tile_group['0'].attrs[k] = ijmetadata[k]
        
                tmp_omero_md = omero_style_metadata(metadata, tile_group.name)
                tile_group.attrs['omero'] = tmp_omero_md 
                tile_group.attrs['micromanager'] = mm_metadata_list[roi_indices[roi][t]]
        
                pbar.update(1)
            
        # Save MM summary metadata
        root_group.attrs['micromanager'] = {'Summary': metadata['Summary']}
        root_group.attrs['_roi_tiles'] = roi_indices
        root_group.attrs['_creator'] = {'name': 'MM2ZARR',
                                        'version': str(__version)}
        
    