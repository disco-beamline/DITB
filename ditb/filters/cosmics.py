#!/usr/bin/python3

"""
This file is part of DITB.
"""

import numpy as np
from scipy.signal import ricker, convolve, windows
from joblib import Parallel, delayed
from skimage import morphology


def remove_cosmic(image: np.array, cosmic_size=2) -> np.array:
    """
    Remove small dots like cosmics imput image using opening filter with
    a disk of size cosmic_size.
    """
    
    disk = morphology.diamond(cosmic_size)
    if len(image.shape) == 2:
        return morphology.opening(image, disk)
        
    out_size = [None] * (len(image.shape) - 2)
    return morphology.opening(image.squeeze(), disk)[*out_size, :, :]
    

def find_spike_1D(spectra1D, threshold=4, apodisation_border=0.2):
    """
    Use a 1d Ricker wavelet to convolve the signal and find peaks above
    median + threshold * std of the convolution value.

    The signal is first apodized using turkey window (to remove peaks in the border
    of the spectra).

    Paramaters:
    -----------

    sectra1D, array:
        Spectral data

    threshold: float:
        Set the threshold to find peaks, it is defined as:
        peak = convolved_signal >= median(convolved_signal) + threshold * std(convolved_signal)

    apodisation_border, float:
        The smoothness of the border of the turkey window, low value include 
        more signal from the edges, higher value increase the damping of edge 
        wavelengths.

    Return:
    -------

    Peak position (array), list of peaks border (left, right)
    """

    # Create a mexican hat (or ricker wavelet)
    kernel = ricker(100, 1)

    # Apodise the signal to remove border effect and do the convolution
    convolved = convolve(spectra1D * windows.tukey(len(spectra1D), alpha=apodisation_border),
                         kernel, 'same')


    # Find indices above 10*std()
    peaks = np.where(convolved >= threshold * np.std(convolved))[0]

    # find peaks borders
    peaks_limits = []
    for p in peaks:
        try:
            linf = np.where(convolved[:p] < 0)[0][-1]
        except:
            linf = p-1
        try:
            lsup = np.where(convolved[p:] < 0)[0][0]+p
        except:
            lsup = p+1

        peaks_limits += [(linf, lsup)]

    return peaks, peaks_limits


def despike_1D(spectra1D, threshold=4, apodisation_border=0.2, iteration=0, 
               max_iteration=20):
    """
    Remove spkies from a 1D signal. This use the find_spike_1d function to find 
    the location of spikes and then interpolate the signal at the position of 
    those spikes. The removing is recursive until no more spikes are found.
   
    Parameters:
    -----------
   
    threshold: float:
        Set the threshold to find peaks, it is defined as:
        peak = convolved_signal >= median(convolved_signal) + threshold * std(convolved_signal)

    apodisation_border, float:
        The smoothness of the border of the turkey window, low value include 
        more signal from the edges, higher value increase the damping of edge 
        wavelengths.
   
    iteration, int:
        keep that at zero.
        
    max_iteration, int:
        The maximum iteration allowed to find spikes.
        
    Return:
    -------
    an array with less spikes
    """
               
    peaks, pl = find_spike_1D(spectra1D, threshold=threshold)
    w = np.arange(0, len(spectra1D))

    # Need to be in two steps
    # First put value of peaks to NaN
    spectra_clean = spectra1D.copy()
    for i, p in enumerate(peaks):
        linf = int(pl[i][0])
        if linf < 0:
            linf = 0
        lsup = int(pl[i][1])
        if lsup > len(w):
            lsup = len(w)-1

        spectra_clean[linf:lsup] = np.NaN

    # Interp those values
    masked_data = np.ma.masked_invalid(spectra_clean)
    spectra_clean[masked_data.mask] = np.interp(w[masked_data.mask], w[~masked_data.mask], spectra_clean[~masked_data.mask])

    if iteration < max_iteration:
        # peaks, pw = find_peaks(spectra_clean, wlen=wlen, prominence=prominence, width=width, rel_height=1)
        peaks, pl = find_spike_1D(spectra_clean, apodisation_border=0.2, threshold=threshold)
        if len(peaks) > 0:
            spectra_clean = despike_1D(spectra_clean, threshold=threshold, apodisation_border=0.2, iteration=iteration+1)
    else:
        if max_iteration != 0:
            print('max iteration reached')

    return spectra_clean


def mean_smooth(signal1D, kernel_size=3, repeat=10):
    """
    Smooth a 1d signal using a moving average
    """
    
    # Smooth using a small kernel window
    mean_kernel = np.full(kernel_size, 1/kernel_size)
    smooth_signal = signal1D

    for r in range(repeat):
        smooth_signal = np.pad(smooth_signal, 2*kernel_size, 'reflect', reflect_type='odd')
        smooth_signal = fftconvolve(smooth_signal, mean_kernel,
                                    mode='same')[int(2*kernel_size):-int(2*kernel_size)]

    return smooth_signal
    
    
def despike_hyperspectral(hyperspectral_array, threshold=4, apodisation_border=0.2, 
                          n_jobs=-1, max_iteration=10):
    """
    Despike on hyperspectra data cube using an 1d function on each spectra.
    
    Parameters:
    -----------
    
    threshold: float:
        Set the threshold to find peaks, it is defined as:
        peak = convolved_signal >= median(convolved_signal) + threshold * std(convolved_signal)

    apodisation_border, float:
        The smoothness of the border of the turkey window, low value include 
        more signal from the edges, higher value increase the damping of edge 
        wavelengths.
        
    n_jobs, int or None:
        The number of processor to use for parralel computation 
        (the default, -1, use all the core). Set to None, no parralel 
        computation is done
        
    max_iteration, int:
        The maximum iteration allowed to find spikes.
        
    Return:
    -------
    
    the hyperspectral cube with less spikes
    """
    
    # Make all xy axis on one direction
    collapsed_xy = hyperspectral_array.to_numpy().reshape(hyperspectral_array.shape[0], 
                                                          hyperspectral_array.shape[1]*hyperspectral_array.shape[2])
    
    # Run the 1D despike for each pixels on the cube (but do that on parrallel to speedup!)
    spectra2 = np.vstack(Parallel(n_jobs=n_jobs)(delayed(despike_1D)(collapsed_xy[:,i]) for i in range(collapsed_xy.shape[1]))).swapaxes(0,1)
    
    # Reshape the datacube 
    dtc = spectra2.reshape(hyperspectral_array.shape)
    
    return dtc
    
