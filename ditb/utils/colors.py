#!/usr/bin/python3

"""
Functions to deal with colors
"""
import re


def wavelength_to_hex(wavelength, gamma=0.8):

    '''This converts a given wavelength of light to an 
    approximate RGB color value. The wavelength must be given
    in nanometers in the range from 380 nm through 750 nm
    (789 THz through 400 THz).
    Based on code by Dan Bruton
    http://www.physics.sfasu.edu/astro/color/spectra.html
    
    Expend for 300 to 750 by chauvet
    '''

    wavelength = float(wavelength)
    if wavelength >= 300 and wavelength <= 440:
        attenuation = 0.3 + 0.7 * (wavelength - 300) / (440 - 300)
        R = ((-(wavelength - 440) / (440 - 300)) * attenuation) ** gamma
        G = 0.0
        B = (1.0 * attenuation) ** gamma
    elif wavelength >= 440 and wavelength <= 490:
        R = 0.0
        G = ((wavelength - 440) / (490 - 440)) ** gamma
        B = 1.0
    elif wavelength >= 490 and wavelength <= 510:
        R = 0.0
        G = 1.0
        B = (-(wavelength - 510) / (510 - 490)) ** gamma
    elif wavelength >= 510 and wavelength <= 580:
        R = ((wavelength - 510) / (580 - 510)) ** gamma
        G = 1.0
        B = 0.0
    elif wavelength >= 580 and wavelength <= 645:
        R = 1.0
        G = (-(wavelength - 645) / (645 - 580)) ** gamma
        B = 0.0
    elif wavelength >= 645 and wavelength <= 750:
        attenuation = 0.3 + 0.7 * (750 - wavelength) / (750 - 645)
        R = (1.0 * attenuation) ** gamma
        G = 0.0
        B = 0.0
    else:
        R = 1.0
        G = 1.0
        B = 1.0
        
    R *= 255
    G *= 255
    B *= 255
    
    return "#%02x%02x%02x" % (int(R), int(G), int(B))


def channelName2Color(channel_name: str):
    """
    Function to parse DISCO channel label and to convert them to color
    
    channel_name format:
    DMXXX_WMIN_WMAX_Fluo
    
    """
    
    if 'vis' in channel_name.lower():
        return '#ffffff'
    
    if 'nofilter' in channel_name.lower():
        return '#ffffff'

    digits_in_name = re.findall('([0-9]{3})', channel_name)

    if len(digits_in_name) == 3:
        wmin = int(digits_in_name[1])
        wmax = int(digits_in_name[2])
        wmean = wmin + (wmax - wmin) / 2

        return wavelength_to_hex(wmean)
    
    return '#ffffff'


def merge_channels(dask_array, pixel_size):
    """
    Use the microfilm lib to create RGB merged color image
    TODO
    """

    pass