#!/usr/bin/python3

"""
Functions to convert images from one format to another.
"""
import zarr
from pathlib import Path
from ome_zarr.writer import write_image
from ome_zarr.io import parse_url
from tqdm import tqdm
import dask
from dask.diagnostics import ProgressBar
from tifffile import imwrite

from .metadata import (make_global_metadata, get_pixel_size, get_roi_name_indices, reshapeMetadata,
                       imagej_style_metadata, omero_style_metadata, read_mmzarr_metadata, get_dimensions)
from .imread import mmread, mmzarr_read

try:
    import orjson as json
    orjson = True
except Exception:
    import json
    orjson = False


def MM2Zarr(mm_root_folder: str, destination_folder: str):
    """
    Convert MicroManager seperate file acquisition to an ome-zarr with the following tree:

    Roi:XX
     |--Tile:YY
         |--0
             |-- array(time, channel, z, y, x)

    The metadata of micromanager are alose stored with the same arborescence for each roi/tile

    Parameters:
    -----------

    - mm_root_folder, str:
        The path to the root folder of the MicroManager acquisition

    - destination_folder, str:
        The path to the destination folder
    """

    __version = 0.1

    # Read metadata and images from MicroManager separate file acquisition
    metadata = make_global_metadata(mm_root_folder)
    data = mmread(mm_root_folder, metadata)

    # Get informations from metadata
    pixum = get_pixel_size(metadata)
    # Try to get the depth_step
    try:
        zstepum = metadata['Summary']['z-step_um']
    except:
        zstepum = 1.0

    roi_indices = get_roi_name_indices(metadata)
    scale_omezarr = {'type': 'scale', 'scale': [1.0, 1.0, zstepum, pixum, pixum]}
    mm_metadata_list = reshapeMetadata(metadata)

    # Create the zarr file (remove if already exist)
    new_name = f'{str(Path(destination_folder)/Path(mm_root_folder).name)}.zarr'
    print(f'save to {new_name}')
    # Create a zarr location for the data
    zlocation = parse_url(new_name, 'w')
    root_group = zarr.group(zlocation.store, overwrite=True)

    # The case multi-tile data
    total_roitile = sum([len(roi_indices[k]) for k in roi_indices])
    with tqdm(total=total_roitile) as pbar:
        for roi in roi_indices:
            roi_group = root_group.create_group(f'Roi{roi.replace("roi", "")}')
            for i, t in enumerate(roi_indices[roi]):
                tile_group = roi_group.create_group(f'Tile{i}')
                write_image(data[t], tile_group, None, axes='tczyx',
                            coordinate_transformations=[[scale_omezarr]])

                # For imageJ (support of N5 pluggins metadata, need to add metadata to the root)
                ijmetadata = imagej_style_metadata(metadata, tile_group.name)
                for k in ijmetadata:
                    tile_group['0'].attrs[k] = ijmetadata[k]

                tmp_omero_md = omero_style_metadata(metadata, tile_group.name)
                tile_group.attrs['omero'] = tmp_omero_md
                tile_group.attrs['micromanager'] = mm_metadata_list[t]

                pbar.update(1)

        # Save MM summary metadata
        root_group.attrs['micromanager'] = {'Summary': metadata['Summary']}
        root_group.attrs['_roi_tiles'] = roi_indices
        root_group.attrs['_creator'] = {'name': 'MM2ZARR',
                                        'version': str(__version)}


def mmzarr2MMformat(zarr_folder: str, destinatin_folder: str):
    """
    Convert mmzarr dataset back to micromanager format
    """

    @dask.delayed
    def write_position(file_name, image, metadata=None, resolution=None, imagej=True) -> None:
        """
        Simple function to write a single tiff file with resolution information
        """
        imwrite(file_name, image, imagej=imagej, resolution=resolution, metadata=metadata)

    zarr_folder = Path(zarr_folder)
    data = mmzarr_read(zarr_folder)
    meta = read_mmzarr_metadata(zarr_folder)
    dims = get_dimensions(meta)

    dest_folder = Path(destinatin_folder)
    dest_folder.mkdir(parents=True, exist_ok=True)
    pxsize = get_pixel_size(meta)
    resolution = (1/pxsize, 1/pxsize)

    write_operations = []
    ## Micromanager images and metadata are grouped in all images in one folder per position
    for ir, r in enumerate(meta['_roi_tiles']):
        roi = r.capitalize()
        for it, tile in enumerate(meta['_roi_tiles'][r]):
            mm_meta_format = {'Summary': meta['Summary']}
            for t in range(dims['time']):
                for c in range(dims['channel']):
                    for z in range(dims['z']):
                        im_coord = meta[f'{roi}/Tile{it}'][t,c,z]['coords']
                        im_meta =  meta[f'{roi}/Tile{it}'][t,c,z]['metadata']
                        mm_meta_format[f'Coords-{im_meta["FileName"]}'] = im_coord
                        mm_meta_format[f'Metadata-{im_meta["FileName"]}'] = im_meta

                        # Create the parent folder if it does not exist
                        outfile = dest_folder / Path(im_meta['FileName'])
                        outfile.parent.mkdir(exist_ok=True)

                        write_operations += [
                            write_position(outfile, data[it, t, c, z], resolution=resolution, imagej=True)
                        ]

            with open(outfile.parent / 'metadata.txt', 'wb') as f:
                if orjson:
                    f.write(json.dumps(mm_meta_format, option=json.OPT_INDENT_2 | json.OPT_APPEND_NEWLINE))
                else:
                    f.write(json.dumps(mm_meta_format, indent=1, ensure_ascii=True))

    # Run the task graph of Dask (delayed image operations on new_image_stack: load images, process, save)
    with ProgressBar():
        dask.compute(write_operations)
