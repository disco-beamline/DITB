#!/usr/bin/python3

"""
This file is part of DITB.

Contain functions to stich micro manager tiles using dask
"""

from typing import Dict, List, Tuple, Callable, Union
import dask
from dask.array.core import normalize_chunks
from shapely.strtree import STRtree
from shapely.geometry.base import BaseGeometry
from shapely.geometry.polygon import Polygon
from shapely.geometry import LineString
from scipy.ndimage import affine_transform
from skimage.transform import AffineTransform
from dask.diagnostics import ProgressBar
import numpy as np
from ..io.imread import mmzarr_load_one_tile
from ..io.metadata import (get_imshape, get_pixel_size, list_images, 
                           read_mmzarr_metadata, get_channel_names, 
                           omero_style_metadata, get_tile_coordinate,
                           imagej_style_metadata, get_dimensions,
                           get_affine_transform)
from functools import partial
from pathlib import Path
from ome_zarr import scale, writer
from ome_zarr.io import parse_url
import zarr


def get_tile_corners(width: int, height: int, transform: Union[None, np.array] = None) -> np.array:
    """
    Create the corners of the rectangle define by width and height.
    If transform is given apply this transform matrix to the rectangle 
    corners.
    """

    rect = np.array(((0, 0), (height, 0), 
                     (height, width), 
                     (0, width)))
    
    if transform is not None:
        fake_z = np.concatenate((rect, np.ones((rect.shape[0], 1))), axis=1)
        # Remove the fake Z of the transformer corners
        rect = (transform @ fake_z.T).T[:, :-1]
        
    return rect


def numpy_shape_to_shapely(coords: np.ndarray, shape_type: str = "polygon") -> BaseGeometry:
    """
    Convert an individual shape represented as a numpy array of coordinates
    to a shapely object
    """
    _coords = coords[:, ::-1].copy()  # shapely has col,row order, numpy row,col
    _coords[:, 1] *= -1  # axis direction flipped between shapely and napari
    if shape_type in ("rectangle", "polygon", "ellipse"):
        return Polygon(_coords)
    elif shape_type in ("line", "path"):
        return LineString(_coords)
    else:
        raise ValueError
        

def get_chunk_coordinates(shape: Tuple[int, int], chunk_size: Tuple[int, int]):
    """Iterator that returns the bounding coordinates
    for the individual chunks of a dask array of size
    shape with chunk size chunk_size.
    return_np_slice determines the output format. If True,
    a numpy slice object is returned for each chunk, that can be used
    directly to slice a dask array to return the desired chunk region.
    If False, a Tuple of Tuples ((row_min, row_max+1),(col_min, col_max+1))
    is returned.
    """
    chunksy, chunksx = normalize_chunks(chunk_size, shape=shape)
    return get_chunk_coordinates2(chunksy, chunksx)

        
def get_chunk_coordinates2(chunksy, chunksx):
    """
    Convert chunks boundaries to chunk coordinates
    """
    y = 0
    for cy in chunksy:
        x = 0
        for cx in chunksx:
            yield ((y, y + cy), (x, x + cx))
            x = x + cx
        y = y + cy
        
        
def get_rect_from_chunk_boundary(chunk_boundary):
    """given a chunk boundary tuple, return a numpy
    array that can be added as a shape to napari"
    """
    ylim, xlim = chunk_boundary
    miny, maxy = ylim[0], ylim[1] - 1
    minx, maxx = xlim[0], xlim[1] - 1
    return np.array([[miny, minx], [maxy, minx], [maxy, maxx], [miny, maxx]])


def find_chunk_tile_intersections(
    tiles_infos: list[dict['shapely polygone', 'dask image', 'affine transform']],
    chunks_infos: list[dict['shapely polygone', 'chunk boundaries']],
    ) -> Dict[Tuple[int, int], Tuple[str, np.ndarray]]:
    """
    For each large chunk find tiles whithin this chunk and return a list with the 
    dask lazy image and transform for each tile in that chunks.
    
    Parameters:
    -----------
    tiles_infos, list of dict: 
        Contains list of tile defined as a dict and the dict should have the following keys:
        'shapely': Contains the shapely objects corresponding to transformed tile outlines.
        'lazy_image': The dask object for the given tile
        'transform': The transform matrix for this tile (including shift to origin)
        
    chunks_infos, list of dict: 
        Contains list of final chunks (large tiles) defined as a dict and the dict should have 
        the following keys:
        'shapely': Contains the shapely objects representing dask array chunks.
        'chunk_boundary': containing a tuple of chunk boundaries
        
    Returns:
    --------
         The chunk_to_tiles dictionary, which has the chunk anchor points as keys and tuples of
         image file names and their corresponding affine transform matrix as values.
    """
    chunk_to_tiles = {}
    tile_tree = STRtree([t['shapely'] for t in tiles_infos])

    for chunk in chunks_infos:
        chunk_boundary = chunk["chunk_boundary"]
        anchor_point = (chunk_boundary[0][0], chunk_boundary[1][0])
        intersecting_tiles = tile_tree.query(chunk['shapely'])
        chunk_to_tiles[anchor_point] = [
            ((tiles_infos[t]["lazy_image"], tiles_infos[t]["transform"]))
            for t in intersecting_tiles
        ]
    return chunk_to_tiles


def fuse_func(
    input_tile_info: Dict[
        Tuple[int, int], List[Tuple[dask.array, np.ndarray]]
    ], 
    load_tile_func=None,
    block_info=None,
    data_dtype=np.uint16,
    fuse_math_fun=np.max,
    bad_value = 0 
) -> np.ndarray:
    """
    Fuses the tiles that intersect the current chunk of a dask array using maximum projection.
    Pass this function to dask.array.map_blocks, after partial evaluation of the required
    image_folder and (if needed) optional arguments.
    Returns:
        Array of chunk-shape containing max projection of tiles falling into chunk
    """
    array_location = block_info[None]["array-location"]
    # The anchor point is the key to the input_tile_info dictionary
    anchor_point = (array_location[0][0], array_location[1][0])
    chunk_shape = block_info[None]["chunk-shape"]
    tiles_info = input_tile_info[anchor_point]
    # print(f"Processing chunk at {anchor_point}")
    fused = np.zeros(chunk_shape, dtype=data_dtype) - bad_value
    # print(tiles_info)
    shift = AffineTransform(translation=(-anchor_point[0], -anchor_point[1]))
    for image_representation, tile_affine in tiles_info:
        if load_tile_func is not None:
            im = load_tile_func(**image_representation)
        else:
            im = image_representation
        
        #tmp_tf = tile_affine.copy()
        #tmp_tf[0, 2] = tmp_tf[0, 2] - anchor_point[0]
        #tmp_tf[1, 2] = tmp_tf[1, 2] - anchor_point[1]
        
        tile_shifted = affine_transform(
            im,
            matrix=np.linalg.inv(shift.params @ tile_affine),
            #matrix=np.linalg.inv(tmp_tf),
            output_shape=chunk_shape,
            cval=0,
        )
        # note that the dtype comversion here happens without scaling
        # may want to use one of the skimage.img_as_* functions instead
        stack = np.stack([fused, tile_shifted])
        fused = fuse_math_fun(stack, axis=0)
        fused[fused == bad_value] = 0

    return fused
    
    
def tiles_as_shapely(metadata: dict, selected_tiles: [list, int] = -1, 
                     use_mm_affine = True):
    """
    Transform micromanager tile coordinates as shapely rectangle 
    
    Parameters:
    -----------
    
    - metadata, dict:
        The dictionnary containing all metadata from micro-manager
        
    - selected_tiles: int or list of int:
        The list of indices of tiles to stich. The value of -1 (the default) 
        means all tiles present in metadata.
        
    - use_mm_affine: boolean:
        To use the affine transformation matrix provided by micro-manager 2 in 
        metadata (default is True). If False, simply use the translation information 
        from the stage.
    """  
    
    w, h = get_imshape(metadata)
    pixum = get_pixel_size(metadata)
    img_names = np.array(list_images(metadata))
    
    if selected_tiles == -1:
        i_tiles = np.arange(len(img_names))
    else:
        i_tiles = selected_tiles
    
    if not isinstance(i_tiles, np.ndarray):
        i_tiles = np.array(i_tiles)
        
    img_names = img_names[i_tiles]
    coordinates = np.stack([get_tile_coordinate(tile, metadata) for tile in img_names])
    transforms = [AffineTransform(translation=pos[::-1] / pixum).params for pos in coordinates]
    tiles = [get_tile_corners(w, h, t) for t in transforms]
    # tiles_shapely = [numpy_shape_to_shapely(t) for t in tiles]
    
    # Determine the size of all those tiles
    all_bboxes = np.vstack(tiles)
    all_min = all_bboxes.min(axis=0)
    all_max = all_bboxes.max(axis=0)

    stitched_shape = tuple(np.ceil(all_max - all_min).astype(int))
    print(f'Mosaic size {stitched_shape}')

    # Do we need to use the affine transform from Mirco-Manager metadata
    if use_mm_affine:
        telemos_matrix = get_affine_transform(metadata)
        affine_telemos = AffineTransform(matrix=telemos_matrix)
        afT2 = AffineTransform(scale=1., 
                               shear=affine_telemos.shear, 
                               rotation=-affine_telemos.rotation)
        
        # apply the translation to the minimum of tiles 
        transforms_with_shift = []
        for pos in coordinates:
            tmp_aaft = np.copy(afT2.params)
            tmp_aaft[:-1, 2] = pos[::-1]/pixum - all_min
            transforms_with_shift += [tmp_aaft]
            
    else:
        # Create a new transform to offset the origin to minimum position of tiles
        shift_to_origin = AffineTransform(translation=-all_min)
        transforms_with_shift = [t @ shift_to_origin.params for t in transforms]

        
    shifted_tiles = [get_tile_corners(w, h, t) for t in transforms_with_shift]
    tiles_shifted_shapely = [numpy_shape_to_shapely(s) for s in shifted_tiles]
    
    return tiles_shifted_shapely, stitched_shape, transforms_with_shift


def create_mosaic(zarr_file: str, roi: int = -1, channels: [list, int] = -1, mosaic_chunk_size: [int, int] = (1024, 1024),
                  output_file: str = None, nz: int = -1, scale_z: bool = True, data_dtype=np.uint16, 
                  return_dask_array=False, fuse_math_fun=np.max, fuse_bad_value=0):
    """
    Stitch tiles together to create a stiched large image saved with as pyramid ome-zarr
    
    Parameters:
    -----------
    
    -1, in roi and channels means all of them.
    
    - nz: -1 or int,
        The number of z step in the stack, if -1 (the default) use the number of 
        z step define in the metadata.
    TODO
    """
    
    # Load img_tiles as dask 
    meta = read_mmzarr_metadata(zarr_file)
    roi_tiles = meta['_roi_tiles']
    
    _fuse_func = partial(fuse_func, 
                         load_tile_func=mmzarr_load_one_tile,
                         data_dtype=data_dtype,
                         fuse_math_fun=fuse_math_fun,
                         bad_value=fuse_bad_value)
    
    if roi == -1:
        roi = [int(r.replace('roi', '')) for r in roi_tiles]
    
    if not isinstance(roi, list):
        roi = [roi]
        
    channel_names = get_channel_names(meta)
    if channels == -1:    
        channels = np.arange(len(channel_names))
    
    if not isinstance(channels, np.ndarray):
        channels = np.array(channels)
    
    # Get the dimensions of the input stack
    dim_dict = get_dimensions(meta)
    if nz == -1:
        nz = dim_dict['z']
        
    nt = dim_dict['time']
    
    # Create the output file
    if output_file is None:
        new_name = f'{str(Path("./")/Path(zarr_file).name.replace(".zarr", ""))}_mosaic.zarr'
    else:
        new_name = output_file
        
    print(f'save to {new_name}')
    # Create a zarr location for the data
    zlocation = parse_url(new_name, 'w')
    root_group = zarr.group(zlocation.store, overwrite=True)
    
    # Define the scaler for pyramid downsampling
    scaler = scale.Scaler()
    scaler.downscale = 2
    scaler.method = 'nearest'
    scaler.max_layer = 4
 
    # Define scales 
    pixum = get_pixel_size(meta)
    # Try to get the depth_step
    try:
        zstepum = metadata['Summary']['z-step_um']
    except:
        zstepum = 1.0
        
    scale_omezarr = [[{'type': 'scale', 'scale': [1.0, 1.0, zstepum, pixum, pixum]}]]
    for i, l in enumerate(range(0, scaler.max_layer)):
        factor = ((i+1)*scaler.downscale)
        if scale_z: 
            scale_omezarr += [[{'type': 'scale', 
                               'scale': [1.0, 1.0, zstepum*factor, 
                                        pixum*factor, pixum*factor]}]]
        else:
            scale_omezarr += [[{'type': 'scale', 
                               'scale': [1.0, 1.0, zstepum, 
                                         pixum*factor, pixum*factor]}]]
    
    for r in roi:
        print(f'Stich ROI {r} / {len(roi)}')
        i_tiles = roi_tiles[f'roi{r}']
        
        shapely_tiles, total_size, transforms = tiles_as_shapely(meta, i_tiles)
        chunks = normalize_chunks(mosaic_chunk_size, shape=total_size)
        chunk_boundaries = list(get_chunk_coordinates(total_size, mosaic_chunk_size))
        
        # Version with squared chunks
        #nx, ny = np.ceil(np.array(total_size) / (mosaic_chunk_size)).astype(int)
        #chunks = ((mosaic_chunk_size[0],)*nx, (mosaic_chunk_size[1],)*ny)
        #chunk_boundaries = list(get_chunk_coordinates2(*chunks))

        chunk_shapes = list(map(get_rect_from_chunk_boundary, chunk_boundaries))
        chunks_shapely = [numpy_shape_to_shapely(c) for c in chunk_shapes]
        
        # Loop over target tiles (large chunks) to add their boundarie informations
        large_chunks_infos = []
        for chunk_shapely, chunk_boundary in zip(chunks_shapely, chunk_boundaries):
            large_chunks_infos += [{'shapely': chunk_shapely, 
                                    'chunk_boundary': chunk_boundary}]

        
        t_mosaic = []
        for t in range(nt):
            ch_mosaic = []
            for ch in channels:
                z_mosaic = []
                for z in range(nz):
                    print(f'Stich t {t}, z {z}, channel {channel_names[ch]}')
                    # Loop over shapely tiles to attach the transform matrix to them and the tile image object
                    tiles_infos = []
            
                    tile_pos_in_roi = np.arange(len(i_tiles))
                    for tile_shapely, it, transform in zip(shapely_tiles, tile_pos_in_roi, transforms):
                        if f'Roi{r}/Tile{it}' in meta:
                            tiles_infos += [{'shapely': tile_shapely, 
                                             'lazy_image': {'zarr_path': zarr_file, 'roi': r, 'tile': it, 
                                                            'channel': ch, 'z': z}, 
                                             'transform': transform}]
                        else:
                            print(f'No tile Roi{r}/Tile{it} in dataset')

                
       
                    # Now we need to find intersection of new big tiles and small microscope tiles 
                    # to loade them only when needed
                    chunk_tiles = find_chunk_tile_intersections(tiles_infos, large_chunks_infos)
          
            
                    res = dask.array.map_blocks(_fuse_func,
                                                chunk_tiles,
                                                chunks=chunks,
                                                dtype=data_dtype)
            
                    z_mosaic += [res]
                ch_mosaic += [z_mosaic]
            t_mosaic += [ch_mosaic]
        
        # Compute and save the mosaic inside a Roi:XX parent group
        roi_group = root_group.create_group(f'Roi{r}')
        with ProgressBar():
            writer.write_image(dask.array.stack(t_mosaic), roi_group, 
                               scaler=scaler, axes='tczyx', 
                               coordinate_transformations=scale_omezarr)
        
        # Add omero metadata
        roi_group.attrs['omero'] = omero_style_metadata(meta, f'mosaic-{zarr_file.name}-roi{r}')
        # Add scalling for imagej
        ijmeta = imagej_style_metadata(meta, f'mosaic-{zarr_file.name}_roi{r}')

        #for k in ijmeta:
        #    for scale in roi_group.
        #    roi_group['0']

    if return_dask_array:
        with ProgressBar():
            dask_out = dask.array.stack(t_mosaic)

        return dask_out