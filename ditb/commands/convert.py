#!/usr/bin/python3

"""
This file is part of DITB.

Contains the cli interface for the convert command
"""
import click

@click.command
@click.argument('source', nargs=-1, type=click.Path(exists=True))
@click.option('--destination', type=click.Path(exists=False),
               default = './processed',
               help = "The destination folder to store converted data. The default value is a folder called: ./processed if MM2ZARR convertor is used. ./back_to_MM if ZARR2MM convertor is used")
@click.option('--convertor', type=click.Choice(['MM2ZARR', 'ZARR2MM', 'ZARR2HDF', 'ZARR2Tif'], case_sensitive=False),
              help="Set the convertor to use", default = 'MM2ZARR')
@click.option('--force', is_flag=True, help="Force convert even if the directory is already in the destination folder")
def convert(convertor, source, destination, force):
    """
    📦 Convert data to several format.
    """

    # Do the import in the function to improve cli speed
    from ..io.converter import MM2Zarr, mmzarr2MMformat
    from pathlib import Path
    import shutil

    if convertor in ['ZARR2HDF', 'ZARR2Tif']:
        raise NotImplementedError(f'This convertor {convertor} is not yet implemented, please be patient')

    # Create the destination folder if it does not exist
    destination = Path(destination)
    destination = destination.expanduser()
    destination.mkdir(exist_ok=True)


    if convertor == 'MM2ZARR':
        print(f'🪩 Use {convertor} to convert data to {destination} folder')
        for filein in source:
            if destination.name != filein:
                # Create the output name
                output_file = destination / (Path(filein).name + '.zarr')
                print(f'\n▷ convert {filein} to {output_file}')

                if force:

                    # Remove the directory if it exists
                    if output_file.is_dir():
                        shutil.rmtree(str(output_file))
                    try:
                        MM2Zarr(filein, destination)
                    except Exception as e:
                        print(f'Error for file {filein}')
                        print(e)

                else:
                    if not output_file.is_dir():
                        try:
                            MM2Zarr(filein, destination)
                        except Exception as e:
                            print(f'Error for file {filein}')
                            print(e)

    if convertor == 'ZARR2MM':

        # Change de default destination ./processed to './back_to_MM'
        if destination.name == 'processed':
            destination = destination.parent / 'back_to_MM'

        print(f'🪩 Use {convertor} to convert data to {destination} folder')

        for filein in source:

            # Check that the destination folder is not in the input files liste
            # and that the file end by .zarr
            fin = Path(filein)
            if destination.name != filein and fin.suffix == '.zarr':

                # Create the output destination for this file
                output_dir = destination / fin.name.replace(fin.suffix, '')
                print(f'\n▷ convert {filein} to {output_dir}')

                if force:

                    # Remove the directory if it exists
                    if output_dir.is_dir():
                        shutil.rmtree(str(output_dir))
                    try:
                        mmzarr2MMformat(filein, output_dir)
                    except Exception as e:
                        print(f'Error for file {filein}')
                        print(e)

                else:
                    if not output_dir.is_dir():
                        try:
                            mmzarr2MMformat(filein, output_dir)
                        except Exception as e:
                            print(f'Error for file {filein}')
                            print(e)


    print('\n🪩 Done 🌟🌟🌟')
