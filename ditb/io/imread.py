#!/usr/bin/python3

"""
A function to read images series acquired on DISCO BL.
"""
from pathlib import Path
import dask.array as da
from dask import delayed
import numpy as np
import logging
from tifffile import imread
from typing import List, Callable, Union
from .metadata import make_global_metadata, list_images, get_imtype, get_imshape
import zarr
import dask

pathtype = Union[str, Path]
_log = logging.getLogger(__name__)


def load_image(image_path: pathtype, size: [int, int], dtype: str, modifiers: List[Callable] = None) -> np.ndarray:
    """
    Load image and apply transformation.
    
    Parameters:
    -----------
    
    - image_path: str or Path,
        The path to the image to be loaded
        
    - modifiers: list of Collable function (optional),
        List of function that take the loaded image as input and return a 
        modifier version of this imager as output
        
    - size: list of two int,
        the size of the image to load
    
    - dtype: str,
        The type of bit of image to load
        
    return an image as a numpy array
    """
    
    # Take care of empty images
    if image_path.is_file():
        img = imread(image_path)
    else:
        img = np.zeros(size, dtype=dtype)
    
    if modifiers is not None:
        for m in modifiers:
            img = m(img)
    
    return img
        
        
def mmread(root_folder: str, global_metadata: dict = None, roi: int = None, modifiers: List[Callable] = None) -> da:
    """
    Read micromanager metadata and load images from metadata information as dask.array.

    Parameters
    ----------
    
    - root_folder, str:
        The path to the root folder created by micro manager multi dimensional acquisition

    - global_metadata, dict:
        The global metadata describing the images. If set to None (default), this metadata will be parsed from metadata.txt files
        contained in the root directory and subdirectories

    - roi, int:
        The roi to load. You could use that to only load one roi of the data

    - modifiers, list of callable function:
        Add function that will alter the images after loading it. Those functions should not change the size of the original images.

    return
    ------

    an image stack as dask array
    """
    
    root_folder = Path(root_folder)
    
    # TODO: improve how to handle the path with * in it
    if roi is not None:
        if root_folder.name.replace('*', '') in [f'{roi}-Pos', f'roi{roi}_tile']:
            root_folder = root_folder.parent

    if global_metadata is None:
        metadata = make_global_metadata(root_folder)
    else:
        metadata = global_metadata

    image_names = list_images(metadata, roi)
    
    image_size = get_imshape(metadata)
    image_dtype = get_imtype(metadata)

    def lazy_imread(root_folder, imname, imsize, imdtype):
        return da.from_delayed(delayed(load_image)(root_folder / imname, imsize, imdtype, modifiers), 
                               shape=imsize, dtype=imdtype)

    # Need to to a lazy loop over the array
    nroi_tile, ntime, nchannel, nz = image_names.shape
    rt_stack = [None] * nroi_tile
    for rt in range(nroi_tile):
        t_stack = [None] * ntime
        for t in range(ntime):
            c_stack = [None] * nchannel
            for c in range(nchannel):
                z_stack = [None] * nz
                for z in range(nz):
                    
                    z_stack[z] = lazy_imread(root_folder, image_names[rt, t, c, z],
                                             image_size, image_dtype)
                        
                c_stack[c] = da.stack(z_stack)
            t_stack[t] = da.stack(c_stack)
        rt_stack[rt] = da.stack(t_stack)
    image_data = da.stack(rt_stack)
    
    return image_data


def mmzarr_read(zarr_file: str):
    """
    Read a zarr structure created from MicroManager Dataset with the following structure
    Roi
     -> Tile
         - > array(t,c,z,y,x)
         
    return a dask.array with
    (roi+tile, t, c, z, y, x) dimension
    """
    
    in_zarr = zarr.open(zarr_file, 'r')
    in_zarr_path = Path(zarr_file)
    # get RoiXX
    sorted_roi = sorted(int(t[0][3:]) for t in in_zarr.groups())
    all_imgs = []
    for roi in sorted_roi:
        # get X from TileXX 
        sorted_tiles = sorted(int(t[0][4:]) for t in in_zarr[f'Roi{roi}'].groups())
        for tile in sorted_tiles:
            all_imgs += [dask.array.from_zarr(str(in_zarr_path/f'Roi{roi}/Tile{tile}/0'))]
    
    all_imgs = dask.array.stack(all_imgs)
    return all_imgs


def mmzarr_load_one_tile(zarr_path: str, roi=1, tile=0, time=0, channel=0, z=0) -> dask.array:
    """
    Load only one image for the MMzarr file structure
    """
    
    component = f'Roi{roi}/Tile{tile}/0'
    return dask.array.from_zarr(zarr_path, component=component)[time, channel, z]
