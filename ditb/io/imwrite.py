#!/usr/bin/python3

"""
This file is part of DITB.

Contains all functions and classes to write images
to disk
"""

import numpy as np
from tifffile import imwrite
from pathlib import Path
from shutil import copy2
import dask
from dask.diagnostics import ProgressBar
from tifffile.tifffile import read_mm_header

from ditb.io.imread import mmzarr_read
from .metadata import get_pixel_size, list_images, read_mmzarr_metadata, get_dimensions
from ome_zarr.writer import write_image
from ome_zarr.io import parse_url
from tqdm import tqdm
import zarr



def write_mmformat(source_root_folder: str, destination_root_folder: str,
                   global_metadata: dict, new_image_stack: np.array) -> None:
    """
    Write the modified image_stack in the destination_folder while keeping the same
    data structure as Micro-Manager. This function will also copy metadata to the
    destination_folder

    Parameters:
    -----------

    - source_root_folder: str,
        The root folder of the orginal data (the one create by MicroManager
        multi-dimensional-acquisitions.

    - destionation_root_folder: str,
        The name of the folder where to save the data. If the folder does not exist,
        it will be created!

    - global_metadata: dict,
        The dictionnary containing the metadata merged with the `make_global_metadata`
        function.

    - new_image_stack: array (numpy or dask),
        The array of new image stack, this stack should have not be queezed in
        dimension. The image should have the supported type of tiff (int, uint, float32)
    """

    assert len(new_image_stack.shape) == 6, "The new_image_stack should have 6 dimensions [tile, time, channel, z, y, x]"
    assert new_image_stack.dtype in [np.float32, np.uint, np.uint16], "The new_image_stack does not have the correct data type"

    @dask.delayed
    def write_position(file_name, image, root_dir='./', metadata=None, resolution=None, imagej=True) -> None:
        """
        Simple function to write a single tiff file with resolution information
        """
        fout = Path(root_dir) / Path(file_name)
        fout.parent.mkdir(parents=True, exist_ok=True)
        imwrite(fout, image, imagej=imagej, resolution=resolution, metadata=metadata)

    @dask.delayed
    def copy_metadata(image_names: str, source_root_dir: str, dest_root_dir: str) -> None:
        """
        Copy micromanager metadata.txt file from
        source_path to dest_path
        """

        src = Path(source_root_dir) / Path(image_names).parent / 'metadata.txt'
        dst = Path(dest_root_dir) / Path(image_names).parent / 'metadata.txt'
        copy2(src, dst)

    # List image names from global_metadata
    image_names = list_images(global_metadata)
    # Get the resolution of images from metadata
    res = get_pixel_size(global_metadata)

    # Create an empty list to store delayed operation on images
    tiles, times, channels, slices = image_names.shape
    write_operations = [None] * np.product(image_names.shape) * 2

    # Loop over the stack of images
    cpt = 0
    for ti in range(tiles):
        for t in range(times):
            for c in range(channels):
                for z in range(slices):
                    write_operations[cpt] = write_position(image_names[ti, t, c, z],
                                                           new_image_stack[ti, t, c, z],
                                                           destination_root_folder,
                                                           resolution=(1 / res, 1 / res))
                    write_operations[cpt + 1] = copy_metadata(image_names[ti, t, c, z],
                                                              source_root_folder,
                                                              destination_root_folder)
                    cpt += 2

    # Run the task graph of Dask (delayed image operations on new_image_stack: load images, process, save)
    with ProgressBar():
        dask.compute(write_operations)


def write_mm_zarr(output_path: str, output_data: dask.array, orignal_mm_zarr_data_path: str, copy_metadata=True, processing:str = ''):
    """
    Write data as a zarr with the same structure as the original "orginal_mm_zarr_data_path" data. It will also
    copy the metadata from the "orginal_mm_zarr_data_path" to the new zarr data in the "output_path".

    The output_data should have the same shape as the original data contained in the "orginal_mm_zarr_data_path"
    zarr file.
    """

    # Open the original Zarr file converted with MM2ZARR to get Roi/Tile tree structure
    # and also to be able to copy the metadata to the new zarr
    orignal_mm_zarr_data_path = Path(orignal_mm_zarr_data_path)
    original_zarr = zarr.open(orignal_mm_zarr_data_path, 'r')

    # Get the dict with roi and global indices of tiles for this roi
    roi_tiles = original_zarr.attrs['_roi_tiles']

    # Open the destination
    zlocation = parse_url(output_path, 'w')
    root_group = zarr.group(zlocation.store, overwrite=True)


    sorted_roi_int = sorted(int(t[3:]) for t in roi_tiles.keys())
    total_roitile = sum([len(roi_tiles[k]) for k in roi_tiles])
    with tqdm(total=total_roitile) as pbar:
        for roi in sorted_roi_int:
            roi_group = root_group.create_group(f'Roi{roi}')
            tiles_indices = roi_tiles[f'roi{roi}']
            for t, i_tile in enumerate(tiles_indices):
                tile_group = roi_group.create_group(f'Tile{t}')
                write_image(output_data[i_tile], tile_group, None, axes='tczyx')

                if copy_metadata:
                    for k in original_zarr[f'Roi{roi}/Tile{t}'].attrs:
                        tile_group.attrs[k] = original_zarr[f'Roi{roi}/Tile{t}'].attrs[k]
                    for k in original_zarr[f'Roi{roi}/Tile{t}/0'].attrs:
                        tile_group['0'].attrs[k] = original_zarr[f'Roi{roi}/Tile{t}/0'].attrs[k]

                pbar.update(1)

        if copy_metadata:
            for k in original_zarr.attrs:
                root_group.attrs[k] = original_zarr.attrs[k]

        if 'processing' in root_group.attrs:
            root_group.attrs['processing'] += [processing]
        else:
            root_group.attrs['processing'] = [processing]
