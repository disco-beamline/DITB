#!/usr/bin/python3

"""
This file is part of DITB.
"""

from ..io.imread import mmzarr_read
from ..io.metadata import get_channel_names, read_mmzarr_metadata, get_roi, stack_info
from ..filters.cosmics import remove_cosmic
from ..io.imwrite import write_mm_zarr

import dask.array as da
import numpy as np
from skimage.util import img_as_float, img_as_uint
from typing import Union
from pathlib import Path
import logging
arraytype = Union[np.array, da.Array]
pathtype = Union[str, Path]
_log = logging.getLogger(__name__)


def normalize_illumination_beam(white_image: np.array, dark_of_white_image: np.array = None,
                                limit_min_ratio: bool = True) -> arraytype:
    """
    Normalize the illumination beam. This function will compute:
    Normed_illumintaion = (white_image - dark_of_white_image) / max(white_image - dark_of_white_image)
    
    
    If limit_min_ratio is True (default), compute the minRatio = white_image.min()/max(white_image - dark_of_white_image) 
    and replace Normed_illumintaion < minRatio with minRatio

    Parameters
    ----------
    white_image, array (or dask array):
        the array with white image stack (could be a z-stack)
        
    dark_of_white_image, array (or dask array):
        the array with the dark of white image (i.e. the camera noise for the 
        exposure time used to acquire white images)
        
    return 
    ------
    
    the normalized illumination beam as a float image
    """
    
    float_w = img_as_float(white_image) 
    float_dow = img_as_float(dark_of_white_image)

    diff_w = float_w - float_dow 
    max_w = diff_w.max()
    minRatio = float_w.min() / max_w

    if limit_min_ratio:
        corrected_w = diff_w / max_w
        corrected_w[corrected_w < minRatio] = minRatio
    
    return corrected_w
    
    
def normalize_dark_white(image_stack: arraytype, dark_stack: arraytype, 
                         white_image: arraytype, dark_of_white: arraytype) -> arraytype:                     
    """
    Normalize the image stack using the following normalization:
    
    Normelized_stack = Image_stack - dark_stack / Normalized_illumination
    
    with Normalized_illumination = normalize_illumination_beam(white_image, dark_of_white)
    
    Parameters:
    -----------
    
    image_stack, numpy or dask array:
       The stack of images that need to be normalized
        
    dark_stack, numpy or dask array:
        The stack of darks images, they should have the same number of filters 
        than the image_stack
        
    white_image, numpy or dask array:
        The image of the selected white
        
    dark_of_white, numpy or dask array:
        The dark image of the white image
    """
    
    # dark_stack could be of dimension [filter, y, x] or as the same dimension as 
    if len(dark_stack.shape) == len(image_stack.shape):
        assert image_stack.shape[-4] == dark_stack.shape[-4], f"Number of white filters {image_stack.shape[-4]} is different of number of darks filters {dark_stack.shape[-4]}"
    else:
        assert image_stack.shape[-4] == dark_stack.shape[0], f"Number of white filters {image_stack.shape[-4]} is different of number of darks filters {dark_stack.shape[0]}"
    
    # Check type of image stack and convert it to float
    if image_stack.dtype not in [np.float32, np.float64]: 
        _log.info('Convert image_stack to float')
        if isinstance(image_stack, da.Array):
            image_stack = image_stack.map_blocks(img_as_float, dtype=np.float64)
        else: 
            image_stack = img_as_float(image_stack)
            
    # Check type of normalize_dark_white stack and convert it to float
    if dark_stack.dtype not in [np.float32, np.float64]: 
        _log.info('Convert dark_stack to float')
        if isinstance(dark_stack, da.Array):
            dark_stack = dark_stack.map_blocks(img_as_float, dtype=np.float64)
        else: 
            dark_stack = img_as_float(dark_stack)
            
    # Get the normalize_illumination_beam
    norm_beam = normalize_illumination_beam(white_image, dark_of_white)
    
    # Create the normalisation operation 
    
    # Difference with the dark
    diffdark = image_stack - dark_stack
    
    # If negartive values --> put the to zero (float image range from 0 to 1 when they come from unsigned int)
    diffdark[diffdark < 0] = 0
    
    normed = diffdark / norm_beam

    # Need to clip data so that they will not exceed 1
    normed = normed.clip(0,1)
    
    if normed.dtype != np.uint16: 
        if isinstance(normed, da.Array): 
            normed = normed.map_blocks(img_as_uint)
        else:
            normed = img_as_uint(normed)
            
    return normed
    
    
def normalizeTELEMOS(image_stack: str, dark_stack: str, white_stack: str, dark_of_white: str, white_z = 'center', remove_cosmics = False):
    """
    Function to normalize the data acquired with TELEMOS
    
    Parametes:
    ----------
    
    - image_stack: str,
        The path to the zarr file (folder) containing the MicroManager acquisition (could be obtained using mm2zarr command)
        
    - dark_stack: str,
        The path to the zarr of the darks images (one for each channels of the image stack).
        
    - white_stack: str,
        The path to the zarr of the z-stack of white images.
        
    - dark_of_white: str,
        The path to the zarr of the dark for the white z_stack.
    """
    
    # Read image stack and its metadata
    image_meta = read_mmzarr_metadata(image_stack)
    images = mmzarr_read(image_stack)
    im_channels = get_channel_names(image_meta)
    im_roi = get_roi(image_meta)
    print('Image stack infos')
    print(stack_info(image_meta))
    
    # Read the darks
    darks = mmzarr_read(dark_stack)
    darks_meta = read_mmzarr_metadata(dark_stack)
    dark_channels = get_channel_names(darks_meta)
    print('Dark stack infos')
    print(stack_info(darks_meta))
    
    assert im_channels == dark_channels, 'Image and dark stack have different channels settings !'
    
    # Read the white 
    white = mmzarr_read(white_stack)
    white_meta = read_mmzarr_metadata(white_stack)
    print('White stack infos')
    print(stack_info(white_meta))
    
    # Read the dark of white
    darkofwhite = mmzarr_read(dark_of_white)
    darkofwhite_meta = read_mmzarr_metadata(dark_of_white)
    print('Dark of white infos')
    print(stack_info(darkofwhite_meta))
    
    # Do we need to crop the white, dark_of_white and dark_stack to fit the 
    if white_z == 'center':
        white_z = int(white.shape[3] / 2)
        
    white_crop = white[..., white_z, im_roi[0]:im_roi[0] + im_roi[2], im_roi[1]:im_roi[1] + im_roi[3]]
    darkofwhite_crop = darkofwhite[..., im_roi[0]:im_roi[0] + im_roi[2], im_roi[1]:im_roi[1] + im_roi[3]]
    darks_crop = darks[..., im_roi[0]:im_roi[0] + im_roi[2], im_roi[1]:im_roi[1] + im_roi[3]]
    
    # Apply filters on darks and darks of white to remove spikes
    if remove_cosmics:
        darkofwhite_crop = darkofwhite_crop.map_blocks(remove_cosmic, 2, dtype=np.uint16)
        darks_crop = darks_crop.map_blocks(remove_cosmic, 2, dtype=np.uint16)
    
    # Run the normalize_dark_white function
    normalized_stack = normalize_dark_white(images,
                                            darks_crop,
                                            white_crop,
                                            darkofwhite_crop)
    
    # Export the new corrected data to a new zarr file
    in_path = Path(image_stack)
    out_path = in_path.parent / in_path.name.replace('.zarr', '_normalized.zarr')
    print(f'Save normalized stack to {out_path}')
    
    processing = []
    if remove_cosmics:
        processing += ['remove_cosmic(dark, 2)', 'remove_cosmic(dark_of_white, 2)']
    processing += [f'use white {white_z} in zstack', 'normalize_dark_white']

    write_mm_zarr(out_path, normalized_stack, image_stack,
                  processing=processing)
    
