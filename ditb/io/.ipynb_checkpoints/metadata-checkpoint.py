#!/usr/bin/python3

"""
Read and parse metadata from MicroManager images
"""

from glob import glob
import numpy as np
import joblib
import dask.bag as db
from dask.diagnostics import ProgressBar
from IPython.display import JSON

try:
    import orjson as json
except Exception:
    print('Install orjson to get faster json reader')
    import json
    
from pathlib import Path
import zarr
from ..utils.colors import channelName2Color


def parse_MM_version(version: str) -> tuple:
    """
    Parse version of micro-manager to return a list of number 
    from the string format in metadata
    """
    
    try:
        version_number, build_date = version.split(' ')
    except:
        version_number = version
        build_date = 0
    
    vminor = version_number.split('.')[-1]
    vmajor = version_number.replace('.' + vminor, ' ')
    
    return (float(vmajor), int(vminor), int(build_date))


def load_metadata(filename: str) -> dict:
    """
    Load a text file into a json dict
    
    Parameter:
    ----------
    
    - filename: str,
        The path and filename to the json text file.
        
    return dict representation of the data
    """
    
    data = None
    with open(filename, 'r', encoding='latin1') as f:
        data = f.read()
        
    return json.loads(data)


def make_global_metadata(root_dir: str, njobs: int = -1) -> dict:
    """
    Take a root path of a micromanager mACQ experiments and load 
    all metadata.
    
    Parameters:
    -----------
    - root_dir: str,
        The path of the main (root) folder created by micromanager 
        multidimensional acquisition.
        
    - njobs: int (optional),
        Use joblib to load json data in parallel using njobs worker, if -1 is given (the default) 
        all available core of the computer will be used.
    
    Return:
    -------
    
    dictionnary with all metadata.
    
    Example:
    --------
    
    make_global_metadata('./my_experiment')
    """
    
    # Change root_dir to a Path object
    root_dir = Path(root_dir)

    # Load all json in parralel using dask bag
    file_names = tuple(glob(str(root_dir / '**/metadata.txt'), recursive=True))
    # Old version with joblib
    # json_data = joblib.Parallel(n_jobs=njobs)([joblib.delayed(load_metadata)(fp) for fp in file_names])
    
    print('Parse metadata from json, this could take some times')
    bag = db.from_sequence(file_names, partition_size=200)
    to_compute = bag.map(load_metadata)
    
    with ProgressBar():
        json_data = to_compute.compute()
    
    # Merge json into one file
    metadata = {}

    
    # Find all metadata.txt in subfolders using glob
    for i, jsond in enumerate(json_data):
        fname = file_names[i]
        mm_version = parse_MM_version(jsond['Summary']['MicroManagerVersion'])
        # For mm1.4 need to add a suffix for each 
        # subfolders to FramaKey-0-0-0
        if mm_version[0] == 1.4:
            fname = Path(fname)
            key_prefix = fname.parent.name
            # Remove Summary repetition
            _ = list(jsond[k].pop('Summary', None) for k in jsond if k != 'Summary')
            n_data = {f"Metadata-{key_prefix}/{jsond[k]['FileName']}": jsond[k] for k in jsond if k != 'Summary'}
            n_data['Summary'] = jsond['Summary']
            jsond = n_data

        # Update metadata dict using union operation
        metadata = metadata | jsond
        
    return metadata


def mm_label_format(label: str) -> str:
    """
    Check the format given by micro-manager on each label of images. 
    This label depends on plugins used to created multi-posisiont acquisition.
    
    The following label format are defined
    - 'multi_pos': for manual multi posistion or single snap, label in [PosX, Pos0, Default]
    - 'slide_explorer': for slide explorer multiposition, label in [roiX_tileX]
    - 'grid': for create grid from position manager, label in [X_PosXXX_YYY]
    """

    if isinstance(label, str):
        label = Path(label)
    
    if label.match('[0-9]*-Pos???_???'):
        return 'grid'
    
    if label.match('roi[0-9]*_tile[0-9]*'):
        return 'slide_explorer'
    
    if label.match('Pos[0-9]*') or str(label) == 'Default':
        return 'multi_pos'
    
    return None
 
       
def list_images(full_metadata: dict, roi=None):
    """
    Convert metadata to an array of images path and names, with the following dimension:
    [roi_tile, time, channel, z].
    
    Parameters:
    -----------
    - full_metadata: dict,
        Metadata from micromanager metadata.txt, could be used with merged metadata
        to extract multi-position image list.
      
    - roi: int or None,
        Filter the roi to use on the metadata to load 
        the images. If None (default) all the roi are
        used
        
    Example:
    --------
    
    metadata = make_global_metadata('./mm_images_root/')
    file_paths = list_images(metadata)
    """
    
    # For the MMZarr format
    if is_MM2ZARR(full_metadata):
        if roi is None:
            img_list = tuple(t for t in full_metadata if t.startswith('Roi'))
        else:
            img_list = tuple(t for t in full_metadata if t.startswith(f'Roi:{str(roi)}'))
        
        return img_list
            
    summary = full_metadata['Summary']
    mmversion = parse_MM_version(summary['MicroManagerVersion'])[0] 

    if mmversion == 2.0:
        # Is it a multiposition dataset ? 
        if 'StagePositions' in summary:
            stage_position_labels = [l['Label'] for l in summary['StagePositions']]
        else:
            stage_position_labels = ['Default']
        
    if mmversion == 1.4:
        # Is it a multiposition dataset ? 
        if 'InitialPositionList' in summary:
            stage_position_labels = [l['Label'] for l in summary['InitialPositionList']]
        else:
            stage_position_labels = ['Pos0']

    if roi is not None:
        # Get the format of the label
        mm_format = mm_label_format(stage_position_labels[0])
        if mm_format == 'grid':
            stage_position_labels = [s for s in stage_position_labels if s.startswith(f'{roi}-Pos')]
            
        if mm_format == 'slide_explorer':
            stage_position_labels = [s for s in stage_position_labels if s.startswith(f'roi{roi}_tile')]
    
    # Extract unique file names (not ordered)
    char_max_size = max((len(k) for k in full_metadata))
    unique_file_names = np.empty((len(stage_position_labels), summary['Frames'], summary['Channels'], summary['Slices']), dtype=f'U{char_max_size}')
    for i, label in enumerate(stage_position_labels):
        tile_roi_file_names = np.array(tuple(label + '/' + k.split('/')[-1] for k in full_metadata if k.startswith(f'Metadata-{label}/')))
        if len(tile_roi_file_names) > 0:
            unique_file_names[i] = np.swapaxes(tile_roi_file_names.reshape(summary['Slices'], summary['Channels'], summary['Frames']), 0, 2)
        else:
            print(f'No image for {label}')
        
        
    return unique_file_names


def get_roi(metadata: dict) -> list:
    """
    Extract the roi from metadata dict of MicroManager
    """
    
    if is_MM2ZARR(metadata):
        return [int(i) for i in metadata['Roi:1/Tile:0'][0,0,0]['metadata']['ROI'].split('-')]
        
    summary = metadata['Summary']
    version = parse_MM_version(summary['MicroManagerVersion'])
    
    if version[0] == 1.4:
        return summary['ROI']
    else:
        for k in metadata:
            if k.startswith('Metadata-'):
                return [int(i) for i in metadata[k]['ROI'].split('-')]
  


def get_imshape(metadata: dict) -> list:
    """
    Extract image shape (width, height) from MicroManager metadata
    """
    
    if is_MM2ZARR(metadata):
        k = tuple(metadata.keys())[0]
        return [metadata[k][0,0,0]['metadata']['Width'], metadata[k][0,0,0]['metadata']['Height']]
    
    summary = metadata['Summary']
    version = parse_MM_version(summary['MicroManagerVersion'])
    
    if version[0] == 1.4:
        return [summary['Width'], summary['Height']]
    else:
        for k in metadata:
            if k.startswith('Metadata-'):
                return [metadata[k]['Width'], metadata[k]['Height']]
  

def get_imtype(metadata: dict) -> str:
    """
    Return the dtype of images uint8 or uint16 from MicroManager metadata
    """
    
    if metadata['Summary']['PixelType'] == 'GRAY16':
        return 'uint16'
    
    return 'uint8'
    

def get_tile_coordinate(image_name: str, metadata: dict) -> list[float, float]:
    """
    Get the coordinate for a given image_name
    
    Parameters:
    -----------
    
    image_name, str:
        The name of the image as defined by MicroManager. 
        For exemple for MM 2: roi16_tile1/img_channel000_position000_time000000000_z000.tif.
        This name could be obtain using list_images function
        
    metadata, dict:
        The dictionnary containing all metadata of MicroManager dataset
        
    Return:
    -------
    A list with X and Y of the image
    """
    
    if is_MM2ZARR(metadata):
        X = metadata[image_name][0,0,0]['metadata']['XPositionUm']
        Y = metadata[image_name][0,0,0]['metadata']['YPositionUm']
        
        return (X, Y)
    
    im_key = None 
    for k in metadata:
        if k.startswith(f'Metadata-{image_name}'):
            im_key = k
            break
            
    if im_key is not None:
        X = float(metadata[im_key]['XPositionUm'])
        Y = float(metadata[im_key]['YPositionUm'])
    
        return (X, Y)
    
    return (0, 0)
    

def is_MM2ZARR(metadata: dict) -> bool:
    return '_creator' in metadata and metadata['_creator']['name'] == 'MM2ZARR'
    

def get_pixel_size(metadata: dict) -> float:
    """
    Get the pixel size in µm/px from MicroManager metadata
    """
    
    # MMZARR metadata
    if is_MM2ZARR(metadata):
        k = tuple(metadata.keys())[0]
        return metadata[k][0,0,0]['metadata']['PixelSizeUm']
        
    # MicroManager standard format
    summary = metadata['Summary']
    version = parse_MM_version(summary['MicroManagerVersion'])
    
    if version[0] == 1.4:
        return summary['PixelSize_um']
    
    else:
        for k in metadata:
            if k.startswith('Metadata-'):
                return metadata[k]['PixelSizeUm']
    
    return 1
    
    
def display_metadata(metadata: dict) -> JSON: 
    """
    Utility function to display metadata nicely in a jupyter notebook
    """
    
    return JSON(metadata)
    
    
def get_z_array(global_metadata: dict) -> np.array:
    """
    Create the z array from metadata
    """
  
    # get the file names
    fnames = list_images(global_metadata)
    
    z = np.empty(len(fnames[0, 0, 0, :]))
    for i, zname in enumerate(fnames[0, 0, 0, :]):
        key = f'Metadata-{zname}'
        z[i] = global_metadata[key]['ZPositionUm']
        
    return z



def get_channel_names(global_metadata: dict) -> list:
    """
    Return the list of channel names used in acquisition
    """

    return global_metadata['Summary']['ChNames']


def omero_style_metadata(global_metadata: dict, name: str, id_omero=1, version="0.5-dev") -> dict:
    """
    Return a dictionnary with channel metadata formated 
    for omero
    
    Refs:
    -----
    https://ngff.openmicroscopy.org/latest/#omero-md
    
    """
    
    channel_names = get_channel_names(global_metadata)
    md_omero = {"id": id_omero,              # ID in OMERO (TODO !!!)
                "name": name,                # Name as shown in the UI
                "version": version, 
                "channels": [],
              }
    
    for chname in channel_names:
        active = True
        end = 5000
        if 'vis' in chname.lower():
            active = False
            end = 30000
            
        md_omero['channels'] += [{
            "active": active,
            "coefficient": 1,
            "color": channelName2Color(chname)[1:].upper(),
            "family": "linear",
            "inverted": False,
            "label": chname,
            "window": {
                "end": 5000,
                "max": 65535,
                "min": 0,
                "start": 0
                }
            }]
        
    return md_omero
        
    
def imagej_style_metadata(global_metadata: dict, name: str) -> dict:
    """
    Create metadata structure compatible with imagej
    """
    
    pixum = get_pixel_size(global_metadata)
    version = parse_MM_version(global_metadata['Summary']['MicroManagerVersion'])

    metadata = {}
    metadata['pixelWidth'] = pixum
    metadata['pixelHeight'] = pixum
    metadata['pixelDepth'] = pixum
    metadata['pixelUnit'] = 'µm'
    if version[0] == 1.4:
        metadata["numChannels"] = global_metadata['Summary']['Channels']
        metadata["numFrames"] = global_metadata['Summary']['Frames']
        metadata["numSlices"] = global_metadata['Summary']['Slices']
    else:
        metadata["numChannels"] = global_metadata['Summary']['IntendedDimensions']['channel']
        metadata["numFrames"] = global_metadata['Summary']['IntendedDimensions']['time']
        metadata["numSlices"] = global_metadata['Summary']['IntendedDimensions']['z']
        
    metadata["frameInterval"] = 0.5
    metadata["yOrigin"] = 0.0
    metadata["fps"] = 2.0
    metadata["imageProperties"] = {}
    metadata["title"] = name
    metadata["xOrigin"] = 0.0
    metadata["ImagePlusType"] = 1
    metadata["zOrigin"] = 0.0
    
    return metadata
        
    
def reshapeMetadata(global_metadata: dict) -> tuple :
    """
    Reshape micromanager metadata so that they will become accessible as a list of list
    
    return metadata
    """
    
    metadata_list = []
    coords_list = []
    tile_names = list_images(global_metadata)
    ntile, ntime, nchannel, nz = tile_names.shape
    version = parse_MM_version(global_metadata['Summary']['MicroManagerVersion'])
    
    for p in range(ntile):
        tmp_meta_t = []
        for t in range(ntime):
            tmp_meta_c = []
            for c in range(nchannel):
                tmp_meta_z = []
                for z in range(nz):  
                    if len(tile_names[p,t,c,z]) > 0:
                        if version[0] == 1.4:
                            # TODO: need to be checked, this is an updata of MM1 metadata to create the same Coords fields that in MM2
                            tmp_coords = {'Frame': 0,
                                          'FrameIndex': t,
                                          'PositionIndex': p,
                                          'Slice': 0,
                                          'SliceIndex': z,
                                          'ChannelIndex': c}
                        else:
                            tmp_coords = global_metadata['Coords-%s' % tile_names[p,t,c,z]]
                        
                        tmp_meta_z += [ {'metadata': global_metadata['Metadata-%s' % tile_names[p,t,c,z]],
                                         'coords': tmp_coords} ]
                    else:
                        tmp_meta_z += [ {'metadata': None,
                                         'coords': {'Frame': 0,
                                                    'FrameIndex': t,
                                                    'PositionIndex': p,
                                                    'Slice': 0,
                                                    'SliceIndex': z,
                                                    'ChannelIndex': c}} ]
                        
                    
                tmp_meta_c += [tuple(tmp_meta_z)]
            tmp_meta_t += [tuple(tmp_meta_c)]
        metadata_list += [tuple(tmp_meta_t)]

    return tuple(metadata_list)


def get_roi_name_indices(global_metadata: dict) -> dict:
    """
    Function to get the list of tile for a given ROI (used when slide_explorer 
    multiroi are used)
    """
    tile_names = list_images(global_metadata)
    roi_name_indices = {}
    cur_roi = 1
    roi_name_indices[f'roi{cur_roi}'] = []
    for i, name in enumerate(tile_names[:,0,0,0]):
        pname = Path(name)
        if pname.parent.match('roi[0-9]*_*'):
            roi_N = int(str(pname.parent).split('_')[0].replace('roi', ''))
        
            if roi_N != cur_roi:
                cur_roi = roi_N
                roi_name_indices[f'roi{cur_roi}'] = []

            roi_name_indices[f'roi{cur_roi}'] += [i]
    
    cleaned_roi_name = {}
    for i, k in enumerate(roi_name_indices):
        if len(roi_name_indices[k]) > 0:
            cleaned_roi_name[f'roi{i+1}'] = roi_name_indices[k]
        
    # If no roi is in the dataset create a dummy one to get the same structure
    if len(cleaned_roi_name) == 0:
        cleaned_roi_name['roi1'] = [i for i in range(tile_names.shape[0])]
        
    return cleaned_roi_name


def read_mmzarr_metadata(mmzar_file: str) -> dict:
    """
    Read all micromanager metadata from a zarr file
    """
    
    in_zarr = zarr.open(mmzar_file, 'r')
    metadata = {}
    # List the roi
    sorted_roi = sorted(int(t[0].split(':')[-1]) for t in in_zarr.groups())
    for roi in sorted_roi:
        # list tiles
        sorted_tiles = sorted(int(t[0].split(':')[-1]) for t in in_zarr[f'Roi:{roi}'].groups())
        
        for tile in sorted_tiles:
            path = f'Roi:{roi}/Tile:{tile}'
            metadata[f'{path}'] = np.array(in_zarr[path].attrs['micromanager'])
            
            
    metadata['Summary'] = in_zarr.attrs['micromanager']['Summary']
    metadata['_creator'] = in_zarr.attrs['_creator']
    metadata['_roi_tiles'] = in_zarr.attrs['_roi_tiles']
    
    return metadata
    

def stack_info(metadata: dict):
    """
    Utility function to print stack information from metadata
    """
    
    roi = get_roi(metadata)
    channels = get_channel_names(metadata)
    
    return f'-camera roi: {roi}\n-channels: {channels}'