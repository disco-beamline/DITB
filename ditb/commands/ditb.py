#!/usr/bin/python3

"""
This file is part of DITB.

Contains the main interface to the command line commands that could be used.
"""
import click
from ditb.commands.convert import convert 
from ditb.commands.darkwhite import darkwhite
from ditb.commands.stitch import stitch

@click.group()
@click.option('--debug/--no-debug', default=False)
@click.pass_context
def cli(ctx, debug):
    """
    Disco Imaging ToolBox (DITB) provides a set of command to work with
    images acquired on DISCO Beamline of the Synchrotron Soleil.
    """
    ctx.ensure_object(dict)
    ctx.obj['version'] = 'pre-alpha'
    ctx.obj['debug'] = debug
    print(f"🪩 Starting DITB {ctx.obj['version']}")
    pass
    

cli.add_command(convert)
cli.add_command(darkwhite)
cli.add_command(stitch)


if __name__ == '__main__':
    cli(obj={}, auto_envvar_prefix='DITB')
    
