"""
This is a minimal python script to open a mosaic of 
hyperspectral image into GlueViz
"""

# FIRST, open GlueViz app 

# The open a terminel in GlueViz an paste the following content
# You will need to adapt the path to your data !

import zarr 
from glue.core import Data

my_hyperspectra_mosaic = "./path/to/excitation_scanning_normalisez_mosaic.zarr"

data = zarr.open(my_hyperspectra_mosaic)['/Roi1/0'][0,0]

data_collection['exscann'] = Data(label='exscann', exscann = data)

