#!/usr/bin/python3

from ditb.exposure.normalisation import normalizeTELEMOS
from ditb.io.imread import mmzarr_read
from ditb.io.imwrite import write_mm_zarr
from ditb.io.metadata import get_channel_names, read_mmzarr_metadata, stack_info, get_roi, get_pixel_size, get_exposure_time, get_mono_position
from ditb.filters.cosmics import remove_cosmic
from skimage import img_as_float, img_as_uint
import dask
from dask.diagnostics import ProgressBar
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt


def correct_dark_white(images: str, dark: str = '', white: str = '' ,
                       darkofwhite: str = '', white_z: [int, str] = 'center',
                       lower_percentile: float = 1, smoothsize: int = 2,
                       remove_cosmics = False, version=2,
                       original_white = None):
    """
    Do the dark white correction of images.

    image_corr = images - dark / (white - darkofwhite)

    If the data are from excitation scanning mode, a first step is to
    estimate the power from the mean of the white stack (without cropping
    and removing the dark). This power estimation is then normed to 1 and divded
    to the data (after data dark substraction)

    parameters
    ----------

    - images: str,
        the path to the zarr file containing the image stack
    - dark: str,
        the path to the dark images of the stack
    - white: str,
        the path to the white
    - darkofwhite: str,
        the path to the dark of white images
    - white_z: int or 'center':
        The plane in the z-stack of the white to use in the correction.
        This option is only used for regular stack correction
    - remove_cosmics: bool:
        Use and opening/closing filter to remove cosmics on the image
    - version: int,
        Use different version of the correction (default 2).
        1: The first version of the correction 1step sptatial and spectral
           correction with the white (using the max(over ex wavelength) on
           the white to estimate the power)
        2: 2 step correction, a) divide the data by the estimation of power from
           the mean of the white along the Ex. wavelength b) correct for spatial
           variation of the white (norme to one for each Ex. wavelength)
    """

    whitein = Path(white)
    imagesin = Path(images)
    darkin = Path(dark)
    darkofwhitein = Path(darkofwhite)

    # Check that they are zarr files
    for f in [whitein, imagesin, darkin, darkofwhitein]:
        if not f.name.endswith('.zarr'):
            raise ValueError(f'the file {f} is not a zarr file')

    # Prepare output name
    out_suffix = f'_normalized_v{version}'

    out_path = imagesin.parent / imagesin.name.replace('.zarr', f'{out_suffix}.zarr')

    white = mmzarr_read(str(whitein))
    meta_white = read_mmzarr_metadata(str(whitein))
    dofwhite = mmzarr_read(str(darkofwhitein))
    white_ch_names = get_channel_names(meta_white)
    images = mmzarr_read(str(imagesin))
    meta = read_mmzarr_metadata(str(imagesin))

    # check if we do excitation scanning or not
    focus_motor = meta_white['Roi1/Tile0'][0,0,0]['metadata']['Core-Focus']
    # Add a test on the name of the channels
    # If the channels starts with DM.... it's probably and excitation scanning
    # If the channels starts with EX... it's sure that it's a multi-excitation-position
    channels_start_with_DM = all([a.startswith('DM') for a in meta['Summary']['ChNames']])

    if 'IHR' in focus_motor and channels_start_with_DM and images.shape[3] == white.shape[3]:
        print(f'Excitation scanning normalisation with version {version} of the correction')

        # Normalise the white (between min_ratio and 1)
        white_stack = white[0,0,0,:].map_blocks(img_as_float)
        dow_stack = dofwhite[0,0,0,0].map_blocks(img_as_float)
        if remove_cosmics:
            dow_stack = dow_stack.map_blocks(remove_cosmic, 2, dtype=dow_stack.dtype)

        diffwhite = white_stack - dow_stack
        if version == 2:
            print('Two steps normalisation: Spectral power from the mean, then spatial variation of the white')
            assert original_white is not None, "Please provide a non-smoothed white"
            # Estimation of the power
            owhite = mmzarr_read(str(original_white))[0, 0, 0, :].map_blocks(img_as_float)
            whiteMean = owhite.mean(axis=(-2,-1))
            powerEstimation = whiteMean / whiteMean.max()
            powerEstimation = powerEstimation.compute()

            # Create a normed white with a maximum of 1
            # for each excitation wavelength
            max_white = diffwhite.max(axis=(-2,-1))
            min_ratio = white_stack.min() / max_white
            normed_white = diffwhite / max_white[:, None, None]
            # Cut at the maximum of the minimum ratio
            normed_white[normed_white < min_ratio.max()] = min_ratio.max()
        else:
            print("On step normalisation: spectral and sptatial from the white normed by it's stack maximum")
            max_white = diffwhite.max()
            min_ratio = white_stack.min() / max_white
            normed_white = diffwhite / max_white
            normed_white[normed_white < min_ratio] = min_ratio

        final_white = normed_white.compute()[None, None, :, :]

        # Correct the images with this final_white
        darks = mmzarr_read(str(darkin))
        darks_meta = read_mmzarr_metadata(str(darkin))

        if remove_cosmics:
            darks = darks.map_blocks(remove_cosmic, 2, dtype=np.uint16)

        dark_ch_names = get_channel_names(darks_meta)


        roi_image = get_roi(meta)
        img_ch_names = get_channel_names(meta)

        # Check that darks have the same acquisition time than images
        # If their channel names are different we could match their
        # acquisition time
        if dark_ch_names != img_ch_names:
            new_darks = dask.array.empty((1, 1, images.shape[2], 1, darks.shape[-2], darks.shape[-1]),
                                        dtype=darks.dtype)
            dark_acq_times = np.array([get_exposure_time(darks_meta, i) for i in range(len(dark_ch_names))])

            # find darks with the same exposure time than channels
            # TODO: we should also check that they have the same emission filter
            for i, img_chan in enumerate(img_ch_names):
                img_acq_time = get_exposure_time(meta, i)
                where_is_good_dark = np.argwhere(dark_acq_times == img_acq_time)
                if len(where_is_good_dark) > 0:
                    # Take the firs dark corresponding to image acquisition time
                    good_dark_pos = where_is_good_dark[0][0]
                    new_darks[0,0,i,0] = darks[0,0,good_dark_pos,0]
                else:
                    raise IndexError(f'No dark for this exposure {img_acq_time}')
        else:
            new_darks = darks
            # TODO: check that exposure time are the same

        # Add a fake white for visible (ones everywhere)
        good_channels = []
        vis_pos = -1
        for i, c in enumerate(img_ch_names):
            if 'vis' in c.lower():
                vis_pos = i
                break

        if vis_pos > -1:
            oshape = list(final_white.shape)
            oshape[2] = oshape[2]+1
            n_white = np.ones(oshape, dtype=final_white.dtype)
            n_white[:, :, np.arange(oshape[2]) != vis_pos, :] = final_white
        else:
            n_white = final_white

        # Do we need to crop the darks
        if new_darks.shape[-2] > roi_image[-2] and new_darks.shape[-1] > roi_image[-1]:
            print('Crop darks')
            new_darks = new_darks[..., roi_image[0]:roi_image[0]+roi_image[2], roi_image[1]:roi_image[1]+roi_image[3]]

        # Do we need to crop white
        if n_white.shape[-2] > roi_image[-2] and n_white.shape[-1] > roi_image[-1]:
            print('Crop white')
            n_white = n_white[..., roi_image[0]:roi_image[0]+roi_image[2], roi_image[1]:roi_image[1]+roi_image[3]]

        # Do the normalisation operation
        images = images.map_blocks(img_as_float)
        new_darks = new_darks.map_blocks(img_as_float)

        diff_im_darks = images - new_darks
        diff_im_darks[diff_im_darks < 0 ] = 0

        # Now we conpute find a lower percentile of pixel value value
        # This allow to put a threshold to not divide low pixel by the
        # normed white, that could bring hight value in to the signal
        """
        print(f'Compute the {lower_percentile}% percentile on all tiles')
        pmin = []
        for i in range(diff_im_darks.shape[3]):
            pmint, pmaxt = dask.array.percentile(diff_im_darks[:,0,0,i,...].ravel(), (lower_percentile, 99))
            pmin += [pmint]

        with ProgressBar():
            pmin = dask.compute(*pmin)

        # Compute the baseline has the min pixel values over the white
        baseline = (pmin/n_white.max(axis=(-2,-1))).squeeze()
        """

        # Save an image of the normalised white profile, and the percentile
        # value as a function of the excitation wavelength
        w = [float(get_mono_position(meta_white, i, excitation_as_z=True)) for i in range(n_white.shape[2])]
        """
        plt.figure(figsize=(15,4))
        plt.subplot(131)
        plt.plot(w, n_white.max(axis=(-2,-1)).squeeze())
        plt.xlabel('excitation wavelength')
        plt.ylabel('Max of normalized white')
        plt.gca().set_title('white')

        plt.subplot(132)
        plt.plot(w, img_as_uint(pmin))
        plt.xlabel('excitation wavelength')
        plt.ylabel(f'Pixel value of {lower_percentile}% percentile')
        plt.gca().set_title('min values')

        plt.subplot(133)
        plt.plot(w, img_as_uint(baseline))
        plt.xlabel('excitation wavelength')
        plt.ylabel(f'Min Pixel value of {lower_percentile}% percentile / max white')
        plt.gca().set_title('base line')

        plt.suptitle(str(imagesin.name))
        plt.tight_layout()
        plt.savefig(str(imagesin.parent / imagesin.name.replace('.zarr', '_normalized.pdf')))
        """

        # Compute the transfert function
        if version == 2:
            # divide the data by the power estimation
            diff_im_darks = diff_im_darks / powerEstimation[None, None, None, :, None, None]


        # The transfer function is just the inverse of the white
        tf = 1 / n_white[None, ...]

        # Mask pixel with value lower than the min percentile
        # masked_images = dask.array.ma.masked_less_equal(diff_im_darks, np.array(pmin)[None, None, None, :, None, None])
        masked_images = diff_im_darks

        # normalise image stack
        normed_images = masked_images * tf

        # Need to clip images max to 1 !
        normed_images[normed_images > 1.0] = 1

        # Set the minimum values (masked values) to 0
        # normed_images[dask.array.ma.getmaskarray(masked_images)] = 0

        # Convert image stack back to 16bit int
        normed_images = normed_images.map_blocks(img_as_uint)

        # Run the computation
        print('Normalize the image stack')
        write_mm_zarr(out_path, normed_images, imagesin,
                      processing = 'Correct excitation scanning intensities')

    # The case of multi-excitation as different filters recorded on white
    elif white.shape[2] > 1:
        print('Multi-channel-excitation white normalisation')

        # Is that a multi-z white ?
        if white.shape[3] > 1:
            if white_z == 'center':
                zpos = int(white.shape[3]/2)
            else:
                zpos = int(white_z)
        else:
            zpos = 0


        white_stack = white[0,0,:,zpos].map_blocks(img_as_float)
        dow_stack = dofwhite[0,0,:,0].map_blocks(img_as_float)
        if remove_cosmics:
            dow_stack = dow_stack.map_blocks(remove_cosmic, 2, dtype=dow_stack.dtype)

        diffwhite = white_stack - dow_stack

        if version == 2:
            print('Two steps normalisation: Spectral power from the mean, then spatial variation of the white')
            assert original_white is not None, "Please provide a non-smoothed white"

            # Estimation of the power
            owhite = mmzarr_read(str(original_white))[0, 0, :, zpos].map_blocks(img_as_float)

            # Fin the position of the white
            i_viswhite = [i for i, wchname in enumerate(white_ch_names) if 'vis' in wchname.lower()]
            if len(i_viswhite) > 0:
                print("Their is a visible channel in white, I remove it !")
                owhite = dask.array.delete(owhite, i_viswhite[0], 0)

            whiteMean = owhite.mean(axis=(-2,-1))
            powerEstimation = whiteMean / whiteMean.max()
            powerEstimation = powerEstimation.compute()

            # Create a normed white with a maximum of 1
            # for each excitation wavelength
            max_white = diffwhite.max(axis=(-2,-1))
            min_ratio = white_stack.min() / max_white
            normed_white = diffwhite / max_white[:, None, None]
            # Cut at the maximum of the minimum ratio
            normed_white[normed_white < min_ratio.max()] = min_ratio.max()
        else:
            print("On step normalisation: spectral and sptatial from the white normed by it's stack maximum")
            max_white = diffwhite.max()
            min_ratio = white_stack.min() / max_white
            normed_white = diffwhite / max_white
            normed_white[normed_white < min_ratio] = min_ratio

        final_white = normed_white.compute()[None, None, :, :]

        darks = mmzarr_read(str(darkin))
        darks_meta = read_mmzarr_metadata(str(darkin))
        if remove_cosmics:
            darks = darks.map_blocks(remove_cosmic, 2, dtype=np.uint16)
        dark_ch_names = get_channel_names(darks_meta)

        roi_image = get_roi(meta)
        # list the name of channels of the data stack
        img_ch_names = get_channel_names(meta)

        assert dark_ch_names == img_ch_names, f'dark and images channels are different, dark: {dark_ch_names}, images: {img_ch_names}'

        # Add a fake white for visible (ones everywhere)
        # Create the new white array with the same number of channel than the image stack
        # We use ones so that if we have visible channel in the image stack they will be devided by 1.
        n_white = np.ones(shape=[1, 1, images.shape[2], 1, white.shape[-2], white.shape[-1]], dtype=final_white.dtype)

        # get all the excitation recorded in white channels
        all_white_ex_wavelength = [get_mono_position(meta_white, chan) for chan in range(white.shape[2])]

        # Loop over image channels
        for chan in range(images.shape[2]):
            if 'vis' not in img_ch_names[chan].lower():

                # get the excitation wavelength of the image for this channel
                img_ex_wavelengths = get_mono_position(meta, chan)

                # find the position in the white of this excitation
                i_white_ex = all_white_ex_wavelength.index(img_ex_wavelengths)

                # Print the paired
                print(f'for channel {img_ch_names[chan]} pos {chan} in data use white position {i_white_ex}')

                # Put the white (corrected from the dark of white) value at the good place in the new_white
                n_white[0, 0, chan, 0] = final_white[0, 0, i_white_ex, 0]

        # Do we need to crop the darks
        if darks.shape[-2] > roi_image[-2] and darks.shape[-1] > roi_image[-1]:
            print('Crop darks')
            darks = darks[..., roi_image[0]:roi_image[0]+roi_image[2], roi_image[1]:roi_image[1]+roi_image[3]]

        # Do we need to crop white
        if n_white.shape[-2] > roi_image[-2] and n_white.shape[-1] > roi_image[-1]:
            print('Crop white')
            n_white = n_white[..., roi_image[0]:roi_image[0]+roi_image[2], roi_image[1]:roi_image[1]+roi_image[3]]


        images = images.map_blocks(img_as_float)
        darks = darks.map_blocks(img_as_float)

        diff_im_darks = images - darks
        diff_im_darks[diff_im_darks < 0 ] = 0

        if version == 2:
            # Need to check all wavelength in the dataset
            powerEallfilter = np.ones(images.shape[2])

            # Check if the white has the same number of filter that the images
            if diff_im_darks.shape[2] != len(powerEstimation):
                # If not Need to find the visible to put it's power to one
                i_vis=0
                for chan in range(diff_im_darks.shape[2]):
                    if 'vis' not in img_ch_names[chan].lower():

                        # get the excitation wavelength of the image for this channel
                        img_ex_wavelengths = get_mono_position(meta, chan)

                        # find the position in the white of this excitation
                        i_white_ex = all_white_ex_wavelength.index(img_ex_wavelengths)
                        # Print the paired
                        print(f'for channel {img_ch_names[chan]} pos {chan} in data use the power estimation position {i_white_ex}')

                        powerEallfilter[chan] = powerEstimation[i_white_ex]

                    else:
                        print(f'Power estimation set to 1 for channel {img_ch_names[chan]} pos {chan}')

            # divide the data by the power estimation
            diff_im_darks = diff_im_darks / powerEallfilter[None, None, :, None, None, None]

        # Compute the transfert function

        # The transfer function is just the inverse of the white
        # (i.e. the excitation spectra of the white is flat)
        tf = 1 / n_white

        normed_images = diff_im_darks * tf

        # Need to clip images max to 1 !
        normed_images[normed_images > 1.0] = 1

        normed_images = normed_images.map_blocks(img_as_uint)
        # Run the computation
        write_mm_zarr(out_path, normed_images, imagesin,
                      processing = 'Correct multi-filter excitation intensities')

    # The classical one excitation correction
    else:
        normalizeTELEMOS(imagesin, darkin, whitein, darkofwhitein, white_z, remove_cosmics)


def load_excitation_LM():
    """
    Measurements of the Lame Matthieu's excitation profile

    Measurement done the 29 septembre 2023 on the Fluoromax4 @ SOLEIL
    """

    # The wavelength
    xlm = np.array([240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252,
                    253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265,
                    266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278,
                    279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291,
                    292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304,
                    305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317,
                    318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330,
                    331, 332, 333, 334, 335, 336, 337, 338, 339, 340])

    # The relative fluorescence measured (emission 530nm, slit 19 nm)
    ylm = np.array([1.00e+08, 1.01e+08, 1.01e+08, 1.00e+08, 1.00e+08, 9.97e+07,
                    9.94e+07, 9.88e+07, 9.83e+07, 9.75e+07, 9.71e+07, 9.66e+07,
                    9.57e+07, 9.51e+07, 9.46e+07, 9.38e+07, 9.30e+07, 9.23e+07,
                    9.18e+07, 9.13e+07, 9.05e+07, 9.00e+07, 8.95e+07, 8.86e+07,
                    8.82e+07, 8.74e+07, 8.67e+07, 8.62e+07, 8.54e+07, 8.44e+07,
                    8.35e+07, 8.22e+07, 8.11e+07, 7.97e+07, 7.81e+07, 7.65e+07,
                    7.50e+07, 7.32e+07, 7.15e+07, 6.97e+07, 6.81e+07, 6.63e+07,
                    6.48e+07, 6.31e+07, 6.17e+07, 6.02e+07, 5.87e+07, 5.72e+07,
                    5.57e+07, 5.37e+07, 5.18e+07, 5.09e+07, 4.95e+07, 4.81e+07,
                    4.68e+07, 4.55e+07, 4.42e+07, 4.30e+07, 4.19e+07, 4.09e+07,
                    3.99e+07, 3.90e+07, 3.81e+07, 3.73e+07, 3.66e+07, 3.58e+07,
                    3.52e+07, 3.45e+07, 3.38e+07, 3.33e+07, 3.27e+07, 3.20e+07,
                    3.14e+07, 3.09e+07, 3.06e+07, 3.03e+07, 2.99e+07, 2.95e+07,
                    2.91e+07, 2.86e+07, 2.82e+07, 2.76e+07, 2.71e+07, 2.69e+07,
                    2.68e+07, 2.66e+07, 2.63e+07, 2.61e+07, 2.59e+07, 2.57e+07,
                    2.56e+07, 2.55e+07, 2.54e+07, 2.54e+07, 2.53e+07, 2.53e+07,
                    2.53e+07, 2.53e+07, 2.53e+07, 2.52e+07, 2.51e+07])

    return xlm, ylm
