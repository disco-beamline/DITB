#!/usr/bin/python3

"""
Filter to smooth 2d images
"""
from ditb.io.imread import mmzarr_read
from ditb.io.imwrite import write_mm_zarr
from ditb.filters.cosmics import remove_cosmic
import numpy as np
from scipy import ndimage
from pathlib import Path


def median_filter(image_block, kernel_size:int):
    """
    Run the ndimage.median_filter on a dask block
    """
    out_size = [None] * (len(image_block.shape) - 2)
    return ndimage.median_filter(image_block.squeeze(), kernel_size)[*out_size,...]


def smooth(input_zarr_path: str, median_kernel_size: int = 9, average_kernel_size: int = 29):
    """
    Smooth to the imput zarr array, with a spatial median filter followed by an average_kernel_size
    """

    img = mmzarr_read(input_zarr_path)

    # Remove cosmics
    img_filtered = img.map_blocks(remove_cosmic, 2, dtype=img.dtype)

    # Median filtering
    img_filtered = img_filtered.map_blocks(median_filter, median_kernel_size,
                                           dtype=img.dtype)

    # Average
    img_filtered = img_filtered.map_blocks(ndimage.uniform_filter, average_kernel_size,
                                           dtype=img.dtype)

    return img_filtered


def smooth_to_zarr(input_zarr_path: str, median_kernel_size: int = 9, average_kernel_size: int = 29,
                   suffix='_smooth'):
    """
    Convenient function to smooth and save the result in a new zarr file
    """

    outzarr = smooth(input_zarr_path, median_kernel_size, average_kernel_size)

    output_path = Path(input_zarr_path)
    output_path = output_path.parent / output_path.name.replace('.zarr', f'{suffix}.zarr')


    write_mm_zarr(output_path, outzarr, input_zarr_path,
                  processing=f'Smoothing (smooth(median_kernel_size={median_kernel_size}, average_kernel_size={average_kernel_size}))')
