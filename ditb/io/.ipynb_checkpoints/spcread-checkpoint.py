#!/usr/bin/env python3
from spc_spectra import File
import numpy as np
import xarray as xr


def read_spc(spc_file: str, as_xarray=True):
    """
    Function to read SPC file using the spc_spectra library of python.

    Parameters:
    -----------

    spc_file, string:
        The path and the filename of the spc file to be loaded.

    as_xarray, boolean:
        If True (the default) Return the result as an DataArray object from xarray lib
        (which contains: datacube, x, y, and wavelength vector, and metadata in attrs).
        If False the data are returned as a list of Datacube [w, x, y], x, y, w, metadata_dict

    Returns:
    --------

    a DataArray, or numpy array[w, x, y], w, x, y, metadata
    """

    # Read the spc file
    dataspc = File(spc_file)

    # Parse the data to extract the datacube
    y = []
    x = []
    w = []
    for s in dataspc.sub:
        w += [s.y]
        y += [s.subwlevel]
        x += [s.subtime]

    wavelength = dataspc.x
    x = np.array(x)
    y = np.array(y)

    # Build the datacube as a numpy array
    datacube = np.vstack(w).reshape(len(np.unique(y)), len(np.unique(x)), len(wavelength))
    datacube = datacube.swapaxes(0,2)

    # Get the metadata
    metadata = {}
    for k in dataspc.log_dict:
        metadata[k.decode('latin1')] = dataspc.log_dict[k].decode('latin1')

    if as_xarray:
        hyperspectral_data = xr.DataArray(datacube, coords={'w':wavelength,
                                                            'x':np.unique(x),
                                                            'y': np.unique(y)})
        hyperspectral_data.attrs = metadata
        return hyperspectral_data

    return datacube, wavelength, x, y, metadata
