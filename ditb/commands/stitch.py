#!/usr/bin/python3

"""
This file is part of DITB.

Contains the cli interface for the stitching of tiles
"""
import click


@click.command()
@click.argument('images', nargs=-1, type=click.Path(exists=True))
@click.option('--force', is_flag=True, help="Force the stitching even if the directory is already in the destination folder")
def stitch(images, force):
    """
    🪟 Stitch tiles from a zarr dataset to create a mosaic.
    """

    # Do the import in the function for cli speed
    from ditb.io.metadata import read_mmzarr_metadata
    from ditb.stitching.tiles import create_mosaic
    from pathlib import Path
    import shutil

    print(f'🪩 Stitch data')

    for filein in images:
        fname = Path(filein).name
        output_file = Path(filein).parent / fname.replace('.zarr', '_mosaic.zarr')
        print(f'\n▷ Stitching {filein} to {output_file}')
        meta = read_mmzarr_metadata(filein)

        # Do we need to scale z with down scaling
        scale_z = True
        if 'core-focus' in meta['Roi1/Tile0'][0,0,0]['metadata'] and 'IHR' in meta['Roi1/Tile0'][0,0,0]['metadata']['Core-Focus']:
            scale_z = False

        # Add a failback when metadata are missing
        if any([fname.startswith('EX') for fname in meta['Summary']['ChNames']]):
            scale_z = False

        if force:
            # Remove the directory if it exists
            if output_file.is_dir():
                shutil.rmtree(str(output_file))
            try:
                create_mosaic(Path(filein), output_file=str(output_file), scale_z=scale_z)
            except Exception as e:
                print(f'Error for file {filein}')
                print(e)
        else:
            if not output_file.is_dir():
                try:
                    create_mosaic(Path(filein), output_file=str(output_file), scale_z=scale_z)
                except Exception as e:
                    print(f'Error for file {filein}')
                    print(e)

    print('\n🪩 Done 🌟🌟🌟')
